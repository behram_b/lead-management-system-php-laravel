<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Campaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('campaign_id');
            $table->string('name',256)->nullable();
            $table->string('url',512)->nullable();
            $table->string('subject',32)->nullable();
            $table->string('channel',32)->nullable();
            $table->string('landing_type',32)->nullable();
            $table->string('contacted_type',128)->nullable();
            $table->string('device',32)->nullable();
            $table->string('months',32)->nullable();
            $table->string('language',32)->nullable();
            $table->string('country',32)->nullable();
            $table->string('city',32)->nullable();
            $table->string('material',64)->nullable();
            $table->string('model',64)->nullable();
            $table->string('budget',32)->nullable();
            $table->string('sales',32)->nullable();
            $table->string('conversion_rate',32)->nullable();
            $table->string('count',32)->nullable();
            $table->string('slug',768)->nullable();
            $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
