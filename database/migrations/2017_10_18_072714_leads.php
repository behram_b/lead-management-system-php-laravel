<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Leads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('leads');
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('leads_id');
            $table->string('name_surname');
            $table->string('email')->nullable();
            $table->string('subject')->nullable();
            $table->string('operation_time', 62)->nullable();
            $table->string('city', 62)->nullable();
            $table->string('country', 62)->nullable();
            $table->string('phone', 32);
            $table->text('message')->nullable();
            $table->string('channel')->nullable();
            $table->text('campaign')->nullable();
            $table->boolean('is_called')->nullable()->default(0);
            $table->boolean('is_interviewed')->nullable()->default(0);
            $table->boolean('appointment_status')->nullable()->default(0);
            $table->string('appointment')->nullable();
            $table->boolean('sales_status')->nullable()->default(0);
            $table->string('sales_amount')->nullable();
            $table->string('sales_date', 32)->nullable();
            $table->string('form_id',32)->nullable();
            $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('leads');
    }
}
