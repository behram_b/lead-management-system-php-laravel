<!DOCTYPE html>
<html lang="<?php echo e(config('app.locale')); ?>">

<?php echo $__env->make('backLayout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      	  <div class="col-md-3 left_col">
    	   <?php echo $__env->make('backLayout.sidebarMenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    	  </div>
		    <!-- top navigation -->
	        <div class="top_nav">
	          <div class="nav_menu">
	                 <?php echo $__env->make('backLayout.topMenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	          </div>
	        </div>
	        <!-- /top navigation -->
	        <!-- page content -->
	        <div class="right_col" role="main">
	          
	          		 	<?php echo $__env->yieldContent('content'); ?>
	        </div>
	        <!-- /page content -->
 <?php echo $__env->make('backLayout.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>