<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $__env->yieldContent('title'); ?></title>
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.5/flatly/bootstrap.min.css" rel="stylesheet">
	<link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link href="css/style.css" rel="stylesheet">
	<style>
		body {
			padding-top: 70px;
		}
	</style>
	<?php echo $__env->yieldContent('style'); ?>
</head>
<body>
	
	    
	        
	        
	            
	                
	                
	                
	                
	            
	            
	        

			
				
					
						
						
					
						
						
					
				
			

	    
	

	<div class="container">
		<?php echo $__env->yieldContent('content'); ?>
	</div>

	<hr/>

	<div class="container createby">
	    &copy; <?php echo e(date('Y')); ?>. EstetikInternational <a href="/">Leads Managements System</a>
	    <br/>
	</div>

			<?php echo $__env->yieldContent('scripts'); ?>
</body>
</html>