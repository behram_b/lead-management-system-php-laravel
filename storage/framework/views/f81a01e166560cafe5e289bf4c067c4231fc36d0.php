


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>

<div class="container">
    <p>Welcome to my website...</p>
</div>

<?php echo $__env->make('frontLayout.script.sweetalert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- Include this after the sweet alert js file -->
<?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</body>
</html>

