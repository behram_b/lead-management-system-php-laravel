<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Leads <small>Ekle</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <?php echo e(Form::open(array('url' => route('leads.insert'), 'class' => 'form-horizontal','files' => true))); ?>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Ad-Soyad 
                        </label><span class="">*</span>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="name" class="form-control col-md-7 col-xs-12"  data-validate-length-range="6" data-validate-words="2" name="name" required type="text" placeholder="İsim - soyad giriniz">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" id="email" name="email"  class="form-control col-md-7 col-xs-12" placeholder="Email adres giriniz">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="subject">Konu
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="subject" name="subject"   data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" placeholder="Konu giriniz">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="website">Şehir
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="city" name="city"   placeholder="Şehir giriniz" class="form-control col-md-7 col-xs-12" >
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupation">Ülke
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="occupation" type="text" name="country"  class="optional form-control col-md-7 col-xs-12" placeholder="Ülke giriniz">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label for="password" class="control-label col-md-3">Telefon</label>  <span class="">*</span>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="phone" type="tel" name="phone"  data-validate-length="6,8" class="form-control col-md-7 col-xs-12" required  placeholder="Telefon Giriniz">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Mecra</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="channel" type="text" name="channel"  data-validate-linked="password" class="form-control col-md-7 col-xs-12"  placeholder="Mecra giriniz">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Mesaj
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="textarea" name="message" placeholder="Mesaj giriniz" class="form-control col-md-7 col-xs-12"></textarea>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <a href="<?php echo e(url('dashboard')); ?>" class="btn btn-primary">Vazgeç</a>
                            <input name="leads_id" hidden type="text" value="">
                            <?php echo Form::submit('Kayıt Ekle', ['class' => 'btn btn-success']); ?>

                        </div>
                    </div>
                    <?php echo e(Form::close()); ?>

                </div>
            </div>
        </div>
    </div>

    <?php echo $__env->make('frontLayout.script.sweetalert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>



<!-- Font Awesome -->






<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>