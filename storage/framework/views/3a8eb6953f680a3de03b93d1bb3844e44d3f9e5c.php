<?php $__env->startSection('content'); ?>


    <div class="container-fluid">
        <br><br>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-filter"></i> Filtreler
                        <small></small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <form method="POST" id="search-form" class="form-inline" role="form">
                            
                                
                                
                            
                            <div class="filter form-group">
                                <label for="channel">Mecra</label>
                                <input type="text" class="form-control" name="channel" id="channel">
                            </div>
                            
                                
                                
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                
                            
                            <div class="filter form-group">
                                <label for="campaign">Kampanya</label>
                                <input type="text" class="form-control" name="campaign" id="campaign">

                            </div>
                            <div class="filter form-group">
                                <label for=" channel">Operasyon Zamanı</label>
                                <select name="operation_time" id="operation_time" class="form-control">
                                    <option value="">...</option>
                                    <option value="Hemen">Hemen</option>
                                    <option value="Bu Ay İçinde">Bu ay içinde</option>
                                    <option value="1 Ay Sonra">1 ay sonra</option>
                                    <option value="Gelecek Ay İçinde">Gelecek ay içinde</option>
                                    <option value="2 Ay İçinde">2 ay içinde</option>
                                    <option value="6 Ay İçinde">6 ay içinde</option>
                                    <option value="6 Ay Sonra">6 ay sonra</option>
                                </select>
                            </div>
                            <div class="filter form-group">
                                <label for="score">Skor</label>
                                <select name="score" id="score" class="form-control">
                                    <option value="">...</option>
                                    <option value="1">1 yıldız</option>
                                    <option value="2">2 yıldız</option>
                                    <option value="3">3 yıldız</option>
                                    <option value="4">4 yıldız</option>
                                    <option value="5">5 yıldız</option>
                                </select>
                            </div>
                            <div class="filter input-group form-group" id="datepicker">
                                <input id="date" type="text" class="input-sm form-control" placeholder="yyyy/dd/mm"/>
                                <span id="d_range" class="input-group-addon"><i class="fa fa-chevron-circle-right"></i> Tarih Aralığı</span>
                                <span style="display: none;" id="date1" class="input-group-addon"><i class="fa fa-chevron-circle-left"></i>Tarih</span>
                            </div>
                            <div style="display: none;" class="input-daterange form-group input-group" id="datepicker">
                                <input type="text" class="input-sm form-control" id="from" placeholder="yyyy/dd/mm"/>
                                <span class="input-group-addon">Tarih aralığı</span>
                                <input type="text" class="input-sm form-control" id="to" placeholder="yyyy/dd/mm" />
                            </div>

                            <button  type="submit" class=" col-xs-6 col-sm-6 col-md-4 col-lg-2 pull-right ft_btn btn btn-default">Sonuç</button>

                            <div style="" class="row tile_count">
                                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                    <span ><i class="fa fa-user"></i> Toplam Lead</span>
                                    <div class="count green"><?php echo e($total); ?></div>
                                    <span class="total_bottom"><i class="green">0% </i></span>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                    <span ><i class="fa fa-chevron-circle-up"></i> Bugün</span>
                                    <div class="count green"><?php echo e($today); ?></div>
                                    <span class="total_bottom"><i class="green">0% </i></span>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                    <span ><i class="fa fa-chevron-circle-left"></i> Dün</span>
                                    <div class="count green"><?php echo e($yesterday); ?></div>
                                    <span class="total_bottom"><i class="green">0% </i></span>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                    <span ><i class="fa fa-arrow-circle-down"></i> Bu ay</span>
                                    <div class="count green"><?php echo e($month); ?></div>
                                    <span class="total_bottom"><i class="green">0% </i></span>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                    <span ><i class="fa fa-chevron-circle-right"></i> Geçen ay</span>
                                    <div class="count green"><?php echo e($endofday); ?></div>
                                    <span class="total_bottom"><i class="green">0% </i></span>
                                </div>
                                <div style="display: none" class="col-md-2 col-sm-4 col-xs-6 tile_stats_count c_l">
                                    <span class="count_top"><i class="fa fa-list"></i> Listelenen Lead</span>
                                    <div class="count list red">0</div>
                                    <span class="list_bottom"><i class="red"><i
                                                    class="fa fa-sort-asc"></i>0% </i></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <a href="<?php echo e(route('leads.create')); ?>" class="pull-right btn btn-success">Lead Ekle</a>
            <div class="x_panel">
                <div class="x_content">
                    <table id="datatable-responsive"
                           class="table table-striped table-hover table-bordered dt-responsive nowrap" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Skor</th>
                            <th></th>
                            <th>Ad Soyad</th>
                            <th>Şehir</th>
                            <th>Mecra</th>
                            <th>Konu</th>
                            <th>Operasyon Zamanı</th>
                            <th>İletişim Tercihi</th>
                            <th>Kampanya</th>
                            <th>Eposta</th>
                            <th>Ülke</th>
                            <th>Telefon</th>
                            <th>Mesaj</th>
                            <th>Arandı mı?</th>
                            <th>Görüşme Durumu</th>
                            <th>Randevu Durumu</th>
                            <th>Randevu</th>
                            <th>Satış Durumu</th>
                            <th>Satış Tutarı</th>
                            <th>Satış Tarihi</th>
                            <th>Lead Durumu</th>
                            <th>Form Id</th>
                            <th>Tarih</th>
                            <th>id</th>
                            <th>İşlem</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>

    <script type="text/javascript">


            var selected = [];
            oTable = $('#datatable-responsive').DataTable({

                processing: true,
                serverSide: true,
                responsive: true,
                "pageLength": 50,
                "paging": true,
                select: true,
                "sDom": '<"top"i>rt<"bottom"flp><"clear">',
                language: {
                    select: {
                        rows: {
                            _: "You have selected %d rows",
                            0: "",
                            1: "Yalnızca 1 satır seçildi"
                        }
                    }
                },


                ajax: {
                    url: '<?php echo e(route('datatable/getdata')); ?>',
                    data: function (d) {
                        d.name_surname = $('input[name=name_surname]').val();
//                        d.phone = $('input[name=phone]').val();
                        d.city = $('select[name=city]').val();
                        d.campaign = $('input[name=campaign]').val();
                        d.channel = $('input[name=channel]').val();
                        d.operation_time = $('select[name=operation_time]').val();
                        d.score = $('select[name=score]').val();
                        d.from = $('input[id=from]').val();
                        d.to = $('input[id=to]').val();
                        d.created_at = $('input[id=date]').val();
                        d.communication_preference = $('input[id=communication_preference]').val();


//                        d.to = $('input[id=to]').val();


                    }
                },

                'columnDefs': [{
                    'targets': 1,
                    'searchable': false,
                    'orderable': false,
                    'width': '1%',
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta) {
                        return '<input type="checkbox">';

                    }
                }],


                "order": [[22, "desc"]],
                columns: [
                    {
                        data: 'score', name: 'score',
                        render: function (data, type, row) {
                            if (row["score"] == 0.50 || row["score"] == 0.75) {
//                                $("td:first-child:contains('1')").html()
                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star-half-o fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            } else if (row["score"] == 1.00 || row["score"] == 1.25) {
//                                $("td:first-child:contains('1')").html()
                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            } else if (row["score"] == 1.50 || row["score"] == 1.75) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-half-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            }
                            else if (row["score"] == 2.00 || row["score"] == 2.25) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            } else if (row["score"] == 2.50 || row["score"] == 2.75) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-half-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            }

                            else if (row["score"] == 3.00 || row["score"] == 3.25) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            } else if (row["score"] == 3.50 || row["score"] == 3.75) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-half-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            } else if (row["score"] == 4.00 || row["score"] == 4.25) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            } else if (row["score"] == 4.50 || row["score"] == 4.75) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-half-o "/>';
                            } else if (row["score"] == 5.00) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star"/>';
                            }


                            return data;
                        }
                    },
                    {data: 'checkbox', name: 'checkbox'},
                    {data: 'name_surname', name: 'name_surname'},
                    {data: 'city', name: 'city'},
                    {data: 'channel', name: 'channel'},
                    {data: 'subject', name: 'subject'},
                    {data: 'operation_time', name: 'operation_time'},
                    {
                        data: 'communication_preference', name: 'communication_preference',
                        render: function (data, type, row) {
                            if (row["communication_preference"] == "Telefon") {
                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return '<i style="color:#84B2D1; margin-right: 4px;" class="fa fa-phone fa-lg" aria-hidden="true"></i>' + numberRenderer(data);
                            } else if (row["communication_preference"] == "WhatsApp") {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return '<i style="color:green; margin-right: 4px;" class="fa fa-whatsapp fa-lg" aria-hidden="true"></i>' + numberRenderer(data);
                            } else if (row["communication_preference"] == "Email") {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return '<i style="color:#ff5722; margin-right: 4px;" class="fa fa-envelope fa-lg" aria-hidden="true"></i>' + numberRenderer(data);
                            }

                            return data;
                        }

                    },
                    {data: 'campaign', name: 'campaign'},
                    {data: 'email', name: 'email'},
                    {data: 'country', name: 'country'},
                    {data: 'phone', name: 'phone'},
                    {data: 'message', name: 'message'},
                    {
                        data: 'is_called', name: 'is_called',
                        render: function (data, type, row) {
                            if (row["is_called"] == 1) {
                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return '<i style="color:#4CAF50" class="fa fa-check-square-o fa-lg"/>';
                            } else if (row["is_called"] == 0) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return ' <i style="color:#F44336" class="fa fa-ban fa-lg"/>';
                            }
                            return data;
                        }
                    },
                    {data: 'is_interviewed', name: 'is_interviewed'},
                    {data: 'appointment_status', name: 'appointment_status'},
                    {
                        data: 'appointment', name: 'appointment',

                        render: function (data, type, row) {
                            if (row["appointment"] == 1) {
                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return ' <i style="color:#4CAF50" class="fa fa-check-square-o fa-lg"/>';
                            } else if (row["appointment"] == 0) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return numberRenderer(data) + ' <i style="color:#F44336" class="fa fa-ban fa-lg"/>';
                            }
                            return data;
                        }

                    },
                    {
                        data: 'sales_status', name: 'sales_status',
                        render: function (data, type, row) {
                            if (row["sales_status"] == 1) {
                                return ' <i style="color:#4CAF50" class="fa fa-check-square-o fa-2x"/>';
                            } else if (row["sales_status"] == 0) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return numberRenderer(data) + ' <i style="color:#F44336" class="fa fa-ban fa-2x"/>';
                            }

                            return data;
                        }
                    },
                    {data: 'sales_amount', name: 'sales_amount'},
                    {data: 'sales_date', name: 'sales_date'},
                    {
                        data: 'status_bar', name: 'status_bar',
                        render: function (data, type, row) {
                            if (row["status_bar"] == 0 || 0 == 0 ) {
                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return '<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:25%">25%</div></div>';

                            }
                            return data;
                        }
                    },
                    {data: 'form_id', name: 'form_id'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'leads_id', name: 'leads_id'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });



            $('#date, #channel, #campaign,  select').on("change keyup", function () {
                oTable.draw();
            });

            $('#search-form').on('submit', function (e) {
                oTable.draw();
                e.preventDefault();

            });




    </script>

    <script>
        function confirmDelete(url) {
            var value = confirm("Bu lead silinecek, emin misiniz?");
            if (value == true) {
                location.href = url;
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            setTimeout(function () {
                var total = $("#total").text();
                var end = $("#end").text();
                $(".total").text(total);
                $(".total_bottom").html('<span class="count_bottom"><i class="green">' + end + '<i class="fa fa-sort-asc"></i></i>Satır arasında</span>');

            }, 1000);

            function updateFullName() {

                var total = $("#total").text();
                var end = $("#end").text();

                if (total == 0 && end == 0) {

                    $(".list").text(0);
                    $(".list_bottom").text(0);

                } else {
                    $(".list").text(total);
                    $(".list_bottom").html('<span class="total_bottom"><i class="green">' + total + '<i class="fa fa-sort-asc"></i></i>leads filtrelendi</span>');

                }

            }


            $('.ft_btn').click(function () {
                $(".c_l").show(100);
            });

            $(".ft_btn, #date, #channel, #campaign").click("change keyup", function () {


                setTimeout(function () {
                    updateFullName();
                }, 700);
            });
            updateFullName()
        });


    </script>

    <script type="text/javascript">


        $('#date').datepicker({
            todayBtn: true,
            clearBtn: true,
            language: "tr",
            todayHighlight: true,
            format: "yyyy-mm-dd",
            todayBtn: "linked"
        });


        $('.input-daterange').datepicker({
            todayBtn: true,
            clearBtn: true,
            language: "tr",
            todayHighlight: true,
            format: "yyyy-mm-dd",
            todayBtn: "linked"
        });


        $('#d_range').click(function() {
//
            $('#date').val("");
            $('.input-daterange').show(100);
            $('#date, #d_range').hide(100);
            $('#date1').show(100);
            $("#from").datepicker('show');



        });
        $('#date1').click(function() {
            $('#date, #d_range').show(100);
            $('.input-daterange, #date1').hide(100);

        });



    </script>

    <style>

        .filter input, select {
            border: none !important;
            border-right: none !important;
            border-top: none !important;
            border-left: none !important;
            box-shadow: none !important;
            border-bottom: 1px dashed #bfb8b8 !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            background: none !important;
            color: black !important;
            border-radius: 4px !important;
            border: 1px solid #828282 !important;
        }

        a.paginate_button, a.paginate_active {
            display: inline-block !important;
            margin-left: 2px !important;
            cursor: pointer !important;
        }

        .top {
            color: #777777;
            font-size: 15px;
        }

        #start, #end {
            color: #060606;
            padding: 1px;
        }

        #total {
            color: #4CAF50;
            font-size: 27px;
            word-spacing: 40px !important;
            letter-spacing: 1px;
            border-bottom: 1px dashed #75ba75;
        }

    </style>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>