<?php $__env->startSection('title'); ?>
Kullanıcı Rolü
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
<div class="panel-heading">Kullanıcı Rolü</div>
<div class="panel-body">
    <a href="<?php echo e(url('role/create')); ?>" class="btn btn-success">Yeni Rol</a>
    <div class="table">
        <table class="table table-bordered table-striped table-hover" id="tblroles">
            <thead>
                <tr>
                    <th>ID</th><th>Slug</th><th>Adı</th><th>İşlemler</th>
                </tr>
            </thead>
            <tbody>
           
            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              
                <tr>
                    <td><?php echo e($item->id); ?></td>
                    <td><a href="<?php echo e(url('role', $item->id)); ?>"><?php echo e($item->slug); ?></a></td><td><?php echo e($item->name); ?></td>
                    <td>
                     <a href="<?php echo e(route('user.index',['type='.$item->name])); ?>" class="btn btn-success btn-xs">Kullanıcıları Göster</a>
                        <a href="<?php echo e(url('role/' . $item->id . '/edit')); ?>" class="btn btn-success btn-xs">Güncelle</a>
                        <a href="<?php echo e(url('role/' . $item->id . '/permissions')); ?>" class="btn btn-warning btn-xs">İzinler</a>
                        <?php echo Form::open([
                            'method'=>'DELETE',
                            'url' => ['role', $item->id],
                            'style' => 'display:inline'
                        ]); ?>

                            <?php echo Form::submit('Sil', ['class' => 'btn btn-danger btn-xs deleteconfirm']); ?>

                        <?php echo Form::close(); ?>

                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
 
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#tblroles').DataTable({
                columnDefs: [{
                    targets: [0],
                    visible: false,
                    searchable: false
                    },
                ],
                order: [[0, "asc"]],
            });
        });
     $(".deleteconfirm").on("click", function(){
            return confirm("Bu rolü silmek istediğinize emin misiniz?");
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>