<?php $__env->startSection('title'); ?>
<?php echo e(trans('role.role')); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="panel panel-default">
        <div class="panel-heading"><?php echo e(trans('role.role')); ?> <?php echo e($role->name); ?></div>

        <div class="panel-body">
   <ul>
    <div class="row" >
            <?php echo e(Form::label('slug', trans('role.slug'), ['class' => 'col-md-4 control-label'])); ?>

            <div class="col-md-6">
            <?php echo e($role->slug); ?>

            </div>
        </div>
      <div class="row">
            <?php echo e(Form::label('name', trans('basic.name'), ['class' => 'col-md-4 control-label'])); ?>

            <div class="col-md-6">
            <?php echo e($role->name); ?>

            </div>
        </div>
      

        <div class="row">
        <br>
        <div class="col-md-6 col-md-offset-4">
            <a href="<?php echo e(route('role.index')); ?>" class="btn btn-default"><?php echo e(trans('basic.back')); ?></a>
            </div>
        </div>
    </ul>
    </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>