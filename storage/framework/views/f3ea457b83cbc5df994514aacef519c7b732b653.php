
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Datatables</title>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.3/css/select.bootstrap.min.css">
    <link rel="stylesheet" href="https://editor.datatables.net/extensions/Editor/css/editor.bootstrap.min.css">




    <script src="https://code.jquery.com/jquery-1.12.4.js"> </script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.3/js/dataTables.select.min.js"></script>
    <script src="https://editor.datatables.net/extensions/Editor/js/dataTables.editor.min.js"></script>
    <script src="https://editor.datatables.net/extensions/Editor/js/editor.bootstrap.min.js"></script>


</head>

<body>

<br><br>

<div class="container-fluid">
    <form method="POST" id="search-form" class="form-inline" role="form">

        <div class="form-group">
            <label for="name">Ad-Soyad</label>
            <input type="text" class="form-control" name="name_surname" id="name_surname" placeholder="Aranacak Ad">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" name="email" id="email" placeholder="Aranacak Mail">
        </div>
        <div class="form-group">
            <label for="city">Şehir</label>
            <input type="text" class="form-control" name="city" id="city" placeholder="Aranacak Şehir">
        </div>
        <div class="form-group">
            <label for="city">Konu</label>
            <input type="text" class="form-control" name="subject" id="subject" placeholder="Aranacak Konu">
        </div>
        <div class="form-group">
            <label for="city">Mecra</label>
            <input type="text" class="form-control" name="channel" id="channel" placeholder="Aranacak Mecra">
        </div>
        <div class="form-group">
            <label for="city">Tarih</label>
            <input type="date" class="form-control" name="created_at" id="created_at" placeholder="Aranacak Tarih">
        </div>

        <button type="submit" class="btn btn-primary">Arama</button>
    </form>
    <br><br>
    <table id="data" class="display table-hover table table-striped table-bordered" cellspacing="0">
        <thead>
        <tr>
            <th>Id</th>
            <th>Ad Soyad</th>
            <th>E-posta</th>
            <th>Konu</th>
            <th>Operasyon Zamanı</th>
            <th>Şehir</th>
            <th>Ülke</th>
            <th>Telefon</th>
            <th>Mesaj</th>
            <th>Mecra</th>
            <th>Arandı mı?</th>
            <th>Görüşme Durumu</th>
            <th>Görüşme</th>
            <th>Randevu</th>
            <th>Satış Durumu</th>
            <th>Satış Tutarı</th>
            <th>Satış Tarihi</th>
            <th>Tarih</th>
            <th>İşlem</th>

        </tr>
        </thead>
    </table>



    <script type="text/javascript">
        $(document).ready(function () {
            var oTable = $('#data').DataTable({
                dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                "<'row'<'col-xs-12't>>"+
                "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                processing: true,
                serverSide: true,
                responsive: true,
                select: true,
                ajax: {
                    url: '<?php echo e(route('datatable/getdata')); ?>',

                    data: function (d) {
                        d.name_surname = $('input[name=name_surname]').val();
                        d.email = $('input[name=email]').val();
                        d.city = $('input[name=city]').val();
                        d.subject = $('select[name=subject]').val();
                        d.channel = $('input[name=channel]').val();
                        d.created_at = $('input[name=created_at]').val();
                    }
                },



                    columns: [
                        {data: 'leads_id', name: 'leads_id'},
                        {data: 'name_surname', name: 'name_surname'},
                        {data: 'email', name: 'email'},
                        {data: 'subject', name: 'subject'},
                        {data: 'operation_time', name: 'operation_time'},
                        {data: 'city', name: 'city'},
                        {data: 'country', name: 'country'},
                        {data: 'phone', name: 'phone'},
                        {data: 'message', name: 'message'},
                        {data: 'channel', name: 'channel'},
                        {data: 'is_called', name: 'is_called', "orderable": false,
                                     "searchable": false,
                             "render": function(data,type,row,meta) {
                               var a = '<a onclick="setUserEdit(\''+row.email+'\',\'' + row.givenName + '\',\'' + row.surName + '\')"><i class="fa fa-edit"></i></a>
                                   return a;
                             }},
                        {data: 'is_interviewed', name: 'is_interviewed'},
                        {data: 'appointment_status', name: 'appointment_status'},
                        {data: 'appointment', name: 'appointment'},
                        {data: 'sales_status', name: 'sales_status'},
                        {data: 'sales_amount', name: 'sales_amount'},
                        {data: 'sales_date', name: 'sales_date'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}

                    ]

            });





            $('#search-form').on('submit', function(e) {
                oTable.draw();
                e.preventDefault();
            });
        });
    </script>



</div>




