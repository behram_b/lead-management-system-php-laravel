<?php $__env->startSection('title'); ?>
Login
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class = "container">
  <div class="wrapper">
    <?php if(Session::has('message')): ?>
     <div class="alert alert-<?php echo e((Session::get('status')=='error')?'danger':Session::get('status')); ?> " alert-dismissable fade in id="sessions-hide">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <strong><?php echo e(Session::get('status')); ?>!</strong> <?php echo Session::get('message'); ?>

      </div>
    <?php endif; ?> 
    <?php echo e(Form::open(array('url' => route('login'), 'class' => 'form-horizontal form-signin','files' => true))); ?>    
        <h3 class="form-signin-heading">Leads Management System</h3>
        <div class="logo">
            <img src="images/logo.png" class="img-responsive"  alt="logo">
        </div>
        <?php echo csrf_field(); ?>

        <div class="form-group <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
            <div class="col-sm-12">
                <?php echo Form::text('email', null, ['class' => 'form-control','placeholder '=>'E-mail']); ?>

                <?php echo $errors->first('email', '<p class="help-block">:message</p>'); ?>

            </div>
        </div>
        <div class="form-group <?php echo e($errors->has('password') ? 'has-error' : ''); ?>">
            <div class="col-sm-12">
                 <?php echo Form::password('password', ['class' => 'form-control','placeholder '=>'Şifre']); ?>

                <?php echo $errors->first('password', '<p class="help-block">:message</p>'); ?>

            </div>
        </div>      
       
        <button class="btn btn-lg btn-primary btn-block submit-btn"  name="Submit" value="Login" type="Submit">Giriş yap</button>

        <div class="login-register">
                
                <a href="<?php echo e(url('password/reset')); ?>">Şifremi unuttum</a>
                <?php if($errors->has('global')): ?>
                <span class="help-block danger">
                    <strong style="color:red" ><?php echo e($errors->first('global')); ?></strong>
                </span>
              <?php endif; ?> 
        </div>     
    </form>
    
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>