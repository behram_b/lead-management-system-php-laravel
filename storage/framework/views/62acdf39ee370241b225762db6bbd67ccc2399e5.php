<?php $__env->startSection('content'); ?>


    <div class="container-fluid">
        <form method="POST" id="search-form" class="form-inline" role="form">

            <div class="form-group">
                <label for="name">Ad-Soyad</label>
                <input type="text" class="form-control" name="name_surname" id="name_surname" placeholder="Aranacak Ad">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="Aranacak Mail">
            </div>
            <div class="form-group">
                <label for="city">Şehir</label>
                <input type="text" class="form-control" name="city" id="city" placeholder="Aranacak Şehir">
            </div>
            <div class="form-group">
                <label for="city">Konu</label>
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Aranacak Konu">
            </div>
            <div class="form-group">
                <label for="city">Mecra</label>
                <input type="text" class="form-control" name="channel" id="channel" placeholder="Aranacak Mecra">
            </div>
            <div class="form-group">
                <label for="city">Tarih</label>
                <input type="date" class="form-control" name="created_at" id="created_at" placeholder="Aranacak Tarih">
            </div>
            <button type="submit" class="btn btn-primary">Arama</button>

        </form>
        <br><br>
        <a href="<?php echo e(route('leads_create')); ?>" class="alignright btn btn-success">Lead Ekle</a>

        <table id="data" class="display table-hover table table-striped table-bordered" cellspacing="0">
            <thead>
            <tr>
                <th>Id</th>
                <th>Ad Soyad</th>
                <th>E-posta</th>
                <th>Konu</th>
                <th>Operasyon Zamanı</th>
                <th>Şehir</th>
                <th>Ülke</th>
                <th>Telefon</th>
                <th>Mesaj</th>
                <th>Mecra</th>
                <th>Kampanya</th>
                <th>Arandı mı?</th>
                <th>Görüşme Durumu</th>
                <th>Görüşme</th>
                <th>Randevu</th>
                <th>Satış Durumu</th>
                <th>Satış Tutarı</th>
                <th>Satış Tarihi</th>
                <th>Form Id</th>
                <th>Tarih</th>
                <th>İşlem</th>
            </tr>
            </thead>
        </table>

    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

    <script type="text/javascript">
        $(document).ready(function () {
            var oTable = $('#data').DataTable({
                dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                "<'row'<'col-xs-12't>>"+
                "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: '<?php echo e(route('datatable/getdata')); ?>',

                    data: function (d) {
                        d.name_surname = $('input[name=name_surname]').val();
                        d.email = $('input[name=email]').val();
                        d.city = $('input[name=city]').val();
                        d.subject = $('input[name=subject]').val();
                        d.channel = $('input[name=channel]').val();
                        d.created_at = $('input[name=created_at]').val();
                    }
                },

                "order": [[ 0, "desc" ]],

                columns: [
                    {data: 'leads_id', name: 'leads_id'},
                    {data: 'name_surname', name: 'name_surname'},
                    {data: 'email', name: 'email'},
                    {data: 'subject', name: 'subject'},
                    {data: 'operation_time', name: 'operation_time'},
                    {data: 'city', name: 'city'},
                    {data: 'country', name: 'country'},
                    {data: 'phone', name: 'phone'},
                    {data: 'message', name: 'message'},
                    {data: 'channel', name: 'channel'},
                    {data: 'campaign', name: 'campaign'},
                    {data: 'is_called', name: 'is_called'},
                    {data: 'is_interviewed', name: 'is_interviewed'},
                    {data: 'appointment_status', name: 'appointment_status'},
                    {data: 'appointment', name: 'appointment'},
                    {data: 'sales_status', name: 'sales_status'},
                    {data: 'sales_amount', name: 'sales_amount'},
                    {data: 'sales_date', name: 'sales_date'},
                    {data: 'form_id', name: 'form_id'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},

                    

                ]

            });





            $('#search-form').on('submit', function(e) {
                oTable.draw();
                e.preventDefault();
            });
        });
    </script>


    <script>
        function confirmDelete(url) {
            var value = confirm("Bu lead silinecek, emin misiniz?");
            if (value == true) {
                location.href = url;
            }
        }

    </script>

    
<?php $__env->stopSection(); ?>



<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>