<?php $__env->startSection('content'); ?>


    <div class="x_panel">
        <div class="x_title">
                <h2><i class="fa fa-list-alt"></i> Kampanyalar
                <small></small>
            </h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#"></a>
                        </li>
                        <li><a href="#"></a>
                        </li>
                    </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>

                <table id="crudTable" class=" table table-bordered table-striped display nowrap" cellspacing="0" width="100%">

                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Kampanya Name</th>
                        <th>URL</th>
                        <th>Konu</th>
                        <th>Kanal</th>
                        <th>Landing Tipi</th>
                        <th>İletişim Tipi</th>
                        <th>Cihaz</th>
                        <th>Ay</th>
                        <th>Dil</th>
                        <th>Ülke</th>
                        <th>Şehir</th>
                        <th>Materyal</th>
                        <th>Model</th>
                        <th>Bütçe</th>
                        <th>Satış</th>
                        <th>Dönüşüm Oranı</th>
                        <th>Count</th>
                        <th>Slug</th>
                        <th>Tarih</th>
                        <th>Eylem</th>
                    </tr>
                    </thead>
                </table>


        <?php $__env->stopSection(); ?>

        <?php $__env->startSection('scripts'); ?>

                <script type="text/javascript">

                    $(function () {
                        $('#crudTable').DataTable({
                            processing: true,
                            serverSide: true,
                            responsive: true,
                            "pageLength": 50,
                            "scrollY": "1000px",
                            "scrollX": true,
                            "scrollCollapse": true,
                            "fixedColumns": {
                                "leftColumns": 2
                            },
                            "order": [[0, "desc"]],

                            ajax: '<?php echo route('datatable/campaigns_getdata'); ?>',
                            columns: [
                                {data: 'campaign_id', name: 'campaign_id'},
                                {data: 'name', name: 'name'},
                                {data: 'url', name: 'url'},
                                {data: 'subject', name: 'subject'},
                                {data: 'channel', name: 'channel'},
                                {data: 'landing_type', name: 'landing_type'},
                                {data: 'contacted_type', name: 'contacted_type'},
                                {data: 'device', name: 'device'},
                                {data: 'months', name: 'months'},
                                {data: 'language', name: 'language'},
                                {data: 'country', name: 'country'},
                                {data: 'city', name: 'city'},
                                {data: 'material', name: 'material'},
                                {data: 'model', name: 'model'},
                                {data: 'budget', name: 'budget'},
                                {data: 'sales', name: 'sales'},
                                {data: 'conversion_rate', name: 'conversion_rate'},
                                {data: 'count', name: 'count'},
                                {data: 'slug', name: 'slug'},
                                {data: 'created_at', name: 'created_at'},
                                {data: 'action', name: 'action'}

                            ]
                        });
                    });
                </script>



                <?php echo $__env->make('frontLayout.script.sweetalert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <script>
                    function confirmDelete(url) {
                        var value = confirm("Bu lead silinecek, emin misiniz?");
                        if (value == true) {
                            location.href = url;
                        }
                    }
                </script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>