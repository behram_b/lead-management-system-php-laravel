<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;


class Leads extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'leads_id', 'name_surname', 'exif_thumbnail(filename)' //'email', 'subject', 'phone', 'city', 'country', 'message', 'channel',
    ];

    protected $table = 'leads';

//    public function scopeBetween($query, Carbon $from, Carbon $to)
//    {
//        $query->whereBetween('created_at', [$from, $to]);
//    }

    public $timestamps = false;



    /**
     * The attributes that should be hidden for arrays.
     *
    // * @var array
     */
    // protected $hidden = [
    // 	'password', 'remember_token',
    // ];
}
