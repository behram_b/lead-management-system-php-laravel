<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Campaigns;
use function Symfony\Component\VarDumper\Tests\Fixtures\bar;

class CampaignController extends Controller
{
    function campaign_create(Request $request)
    {


        return view('pages.campaign_create');
    }


    function campaign_insert(Request $request)
    {
        Campaigns::insert([
            "name" => $request->name,
            "url" => $request->url,
            "subject" => $request->subject,
            "channel" => $request->channel,
            "landing_type" => $request->landing_type,
            "device" => $request->device,
            "contacted_type" => $request->contacted_type,
            "months" => $request->months,
            "language" => $request->language,
            "country" => $request->country,
            "city" => $request->city,
            "material" => $request->material,
            "model" => $request->model,
            "budget" => $request->budget,
            "sales" => $request->sales,
            "conversion_rate" => $request->conversion_rate,
            "count" => $request->count,
            "slug" => $request->slug

        ]);

    }

    public function campaign_autocomplate (Request $request) {

        $term = $request->term;
        $items = Campaigns::where('name','like','%'.$term.'%')->get();

        if(count($items) == 0) {
            $search_resualt[] = 'Kampanya bulunamadı';

        }else {
            foreach ($items as $key => $value) {
                $search_resualt[] = $value->name;
            }
        }
        return  $search_resualt;


    }

    public function campaign_edit($id)
    {
        $campaign = Campaigns::where('campaign_id', $id)->first();
        return view('pages.campaign_edit',compact('campaign','id'));
    }

    public function campaign_update(Request $request)
    {
        Campaigns::where("campaign_id", $request->campaign_id)->update($request->all());
        return "Success";
    }

    public function campaign_delete($id)
    {
        Campaigns::where('campaign_id', $id)->delete();
        return back();
    }

    function campaigns () {
        return view('pages.campaigns');
    }





}
