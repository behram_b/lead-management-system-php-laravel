<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Leads;

class HomeController extends Controller
{
    public function home($value='')
    {
    	return view('auth.login');
    }
    public function YourhomePage($value='')
    {
    	return view('home');
    }
    public function dashboard($value='')
    {   $total = DB::table('leads')->count(); // Total
        $today = Leads::where('created_at', '>=', Carbon::now()->today())->count();
        $month = Leads::where('created_at', '>=', Carbon::now()->startOfMonth())->count();

        $y_start = Carbon::yesterday()->format('Y-m-d 00::00');
        $y_end = Carbon::yesterday()->format('Y-m-d 23:59:59');
        $yesterday = DB::table('leads')->whereBetween('created_at', [$y_start, $y_end])->count();



        //last month

        $start = new Carbon('first day of last month');
        $start->startOfMonth();
        $end = new Carbon('last day of last month');
        $end->endOfMonth();

        $last_month = DB::table('leads')->whereBetween('created_at', [$start, $end])->count();

        //monthly average

        $sum =  $month + $last_month;
        $mont_avg = $sum /2;

        //by cities

        // istanbul

        $total_ist = DB::table('leads')
            ->where('campaign', 'like', '%istanbul%')
            ->count();

        $today_ist = DB::table('leads')
            ->whereDate('created_at', Carbon::today())
            ->where('campaign', 'like', '%istanbul%')
            ->count();

        $yesterday_ist = DB::table('leads')
            ->whereDate('created_at', Carbon::yesterday())
            ->where('campaign', 'like', '%istanbul%')
            ->count();

        $month_ist = DB::table('leads')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->where('campaign', 'like', '%istanbul%')
            ->count();

        $last_month_ist = DB::table('leads')
            ->whereBetween('created_at', [$start, $end])
            ->where('campaign', 'like', '%istanbul%')
            ->count();


        // bursa

        $total_brs = DB::table('leads')
            ->where('campaign', 'like', '%bursa%')
            ->count();

        $today_brs = DB::table('leads')
            ->whereDate('created_at', Carbon::today())
            ->where('campaign', 'like', '%bursa%')
            ->count();

        $yesterday_brs = DB::table('leads')
            ->whereDate('created_at', Carbon::yesterday())
            ->where('campaign', 'like', '%bursa%')
            ->count();

        $month_brs = DB::table('leads')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->where('campaign', 'like', '%bursa%')
            ->count();

        $last_month_brs = DB::table('leads')
            ->whereBetween('created_at', [$start, $end])
            ->where('campaign', 'like', '%bursa%')
            ->count();

        // ankara

        $total_ank = DB::table('leads')
            ->where('campaign', 'like', '%ankara%')
            ->count();

        $today_ank = DB::table('leads')
            ->whereDate('created_at', Carbon::today())
            ->where('campaign', 'like', '%ankara%')
            ->count();

        $yesterday_ank = DB::table('leads')
            ->whereDate('created_at', Carbon::yesterday())
            ->where('campaign', 'like', '%ankara%')
            ->count();

        $month_ank = DB::table('leads')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->where('campaign', 'like', '%ankara%')
            ->count();

        $last_month_ank = DB::table('leads')
            ->whereBetween('created_at', [$start, $end])
            ->where('campaign', 'like', '%ankara%')
            ->count();

        //channel

        $fb = DB::table('leads')
            ->where('channel', 'like', '%facebook%')
            ->count();

        $gg = DB::table('leads')
            ->where('channel', 'like', '%google%')
            ->count();

        $ins = DB::table('leads')
            ->where('channel', 'like', '%instagram%')
            ->count();

        $web = DB::table('leads')
            ->where('channel', 'like', '%estetikinternational%')
            ->count();

        $other = DB::table('leads')
            ->where('channel', 'like', '%android%')
            ->count();

        //score

        $five = DB::table('leads')
            ->where('score', 'like', '5%')
            ->count();

        $four = DB::table('leads')
            ->where('score', 'like', '4%')
            ->count();

        $three = DB::table('leads')
            ->where('score', 'like', '3%')
            ->count();

        $two = DB::table('leads')
            ->where('score', 'like', '2%')
            ->count();

        $one = DB::table('leads')
            ->where('score', 'like', '1%')
            ->count();



//        $five = $five * $five;
//        $four = $four * $four;
//        $three = $three * $three;
//        $two = $two * $two;
//        $one = $one * $one;
//
//
//        $score = $five + $four + $three + $two + $one;
//
//        $score = $score/5;

        //operation_time

        $now = DB::table('leads')
            ->where('operation_time', 'like', 'hemen%')
            ->count();

        $this_mo = DB::table('leads')
            ->where('operation_time', 'like', '%bu ay içinde%')
            ->count();


        $last_mo = DB::table('leads')
            ->where('operation_time', 'like', '%gelecek ay içinde%')
            ->count();
        $three_mo = DB::table('leads')
            ->where('operation_time', 'like', '%3 ay içinde%')
            ->count();

        $six_mo = DB::table('leads')
            ->where('operation_time', 'like', '%6 ay içinde%')
            ->count();

        $six_mo_la = DB::table('leads')
            ->where('operation_time', 'like', '%6 ay sonra%')
            ->count();






    	return view('backEnd.dashboard', compact(
    	    'total','today',
            'month',
            'yesterday',
            'last_month',
            'mont_avg',
            'total_ist',
            'today_ist',
            'yesterday_ist','month_ist',
            'last_month_ist',
            'total_brs',
            'today_brs',
            'yesterday_brs',
            'month_brs',
            'last_month_brs',
            'total_ank',
            'today_ank',
            'yesterday_ank',
            'month_ank',
            'last_month_ank',
            'fb',
            'gg',
            'ins',
            'web',
            'other',
            'now',
            'this_mo',
            'last_mo',
            'three_mo',
            'six_mo',
            'six_mo_la',
            'sales_count',
            'one',
            'two',
            'three',
            'four',
            'five'

        ));
    }
}
