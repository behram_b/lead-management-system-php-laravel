@extends('backLayout.app')


@section('content')


    <div class="container-fluid">
        <br><br>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-filter"></i> Filtreler
                        <small>Float left</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <form method="POST" id="search-form" class="form-inline" role="form">
                            <div class="filter form-group">
                                <label for="name_surname">Ad-Soyad</label>
                                <input type="text" class="form-control" name="name_surname" id="name_surname">
                            </div>
                            <div class="filter form-group">
                                <label for="email">Telefon</label>
                                <input type="number" class="form-control" name="phone" id="phone">
                            </div>
                            <div class="filter form-group">
                                <label for="city">Şehir</label>
                                <select name="city" id="city" class="form-control">
                                    <option value="">Seçiniz</option>
                                    <option value="Adana">Adana</option>
                                    <option value="Adıyaman">Adıyaman</option>
                                    <option value="Afyon">Afyon</option>
                                    <option value="Ağrı">Ağrı</option>
                                    <option value="Aksaray">Aksaray</option>
                                    <option value="Amasya">Amasya</option>
                                    <option value="Ankara">Ankara</option>
                                    <option value="Antalya">Antalya</option>
                                    <option value="Ardahan">Ardahan</option>
                                    <option value="Artvin">Artvin</option>
                                    <option value="Aydın">Aydın</option>
                                    <option value="Balıkesir">Balıkesir</option>
                                    <option value="Bartın">Bartın</option>
                                    <option value="Batman">Batman</option>
                                    <option value="Bayburt">Bayburt</option>
                                    <option value="Bilecik">Bilecik</option>
                                    <option value="Bingöl">Bingöl</option>
                                    <option value="Bitlis">Bitlis</option>
                                    <option value="Bolu">Bolu</option>
                                    <option value="Burdur">Burdur</option>
                                    <option value="Bursa">Bursa</option>
                                    <option value="Çanakkale">Çanakkale</option>
                                    <option value="Çankırı">Çankırı</option>
                                    <option value="Çorum">Çorum</option>
                                    <option value="Denizli">Denizli</option>
                                    <option value="Diyarbakır">Diyarbakır</option>
                                    <option value="Düzce">Düzce</option>
                                    <option value="Edirne">Edirne</option>
                                    <option value="Elazığ">Elazığ</option>
                                    <option value="Erzincan">Erzincan</option>
                                    <option value="Erzurum">Erzurum</option>
                                    <option value="Eskişehir">Eskişehir</option>
                                    <option value="Gaziantep">Gaziantep</option>
                                    <option value="Giresun">Giresun</option>
                                    <option value="Gümüşhane">Gümüşhane</option>
                                    <option value="Hakkari">Hakkari</option>
                                    <option value="Hatay">Hatay</option>
                                    <option value="Iğdır">Iğdır</option>
                                    <option value="Isparta">Isparta</option>
                                    <option value="İçel">İçel</option>
                                    <option value="İstanbul">İstanbul</option>
                                    <option value="İzmir">İzmir</option>
                                    <option value="Kahramanmaraş">Kahramanmaraş</option>
                                    <option value="Karabük">Karabük</option>
                                    <option value="Karaman">Karaman</option>
                                    <option value="Kars">Kars</option>
                                    <option value="Kastamonu">Kastamonu</option>
                                    <option value="Kayseri">Kayseri</option>
                                    <option value="Kırıkkale">Kırıkkale</option>
                                    <option value="Kırklareli">Kırklareli</option>
                                    <option value="Kırşehir">Kırşehir</option>
                                    <option value="Kilis">Kilis</option>
                                    <option value="Kilis">Kocaeli</option>
                                    <option value="Konya">Konya</option>
                                    <option value="Kütahya">Kütahya</option>
                                    <option value="Malatya">Malatya</option>
                                    <option value="Manisa">Manisa</option>
                                    <option value="Mardin">Mardin</option>
                                    <option value="Muğla">Muğla</option>
                                    <option value="Muş">Muş</option>
                                    <option value="Nevşehir">Nevşehir</option>
                                    <option value="Niğde">Niğde</option>
                                    <option value="Ordu">Ordu</option>
                                    <option value="Osmaniye">Osmaniye</option>
                                    <option value="Rize">Rize</option>
                                    <option value="Sakarya">Sakarya</option>
                                    <option value="Samsun">Samsun</option>
                                    <option value="Siirt">Siirt</option>
                                    <option value="Sinop">Sinop</option>
                                    <option value="Sivas">Sivas</option>
                                    <option value="Şanlıurfa">Şanlıurfa</option>
                                    <option value="Şırnak">Şırnak</option>
                                    <option value="Tekirdağ">Tekirdağ</option>
                                    <option value="Tokat">Tokat</option>
                                    <option value="Trabzon">Trabzon</option>
                                    <option value="Tunceli">Tunceli</option>
                                    <option value="Uşak">Uşak</option>
                                    <option value="Van">Van</option>
                                    <option value="Yalova">Yalova</option>
                                    <option value="Yozgat">Yozgat</option>
                                    <option value="Zonguldak">Zonguldak</option>
                                </select>
                            </div>
                            <div class="filter form-group">
                                <label for="campaign">Kampanya</label>
                                <input type="text" class="form-control" name="campaign" id="campaign">
                                {{--<select id="subject" name="subject" class="form-control">--}}
                                {{--<option value="">Seçiniz</option>--}}
                                {{--<option value="Microblading">Microblading</option>--}}
                                {{--<option value="Örümcek Ağı Estetiği">Örümcek Ağı Estetiği</option>--}}
                                {{--<option value="Organik Saç Ekimi">Organik Saç Ekimi</option>--}}
                                {{--<option value="Burun Estetiği">Burun Estetiği</option>--}}
                                {{--<option value="Kepçe Kulak Estetiği">Kepçe Kulak Estetiği</option>--}}
                                {{--<option value="Meme Büyütme">Meme Büyütme</option>--}}
                                {{--<option value="Meme Küçültme">Meme Küçültme</option>--}}
                                {{--<option value="Meme Dikleştirme">Meme Dikleştirme</option>--}}
                                {{--<option value="Yüz Germe">Yüz Germe</option>--}}
                                {{--<option value="Göz Kapağı Estetiği">Göz Kapağı Estetiği</option>--}}
                                {{--<option value="Göz Altı Morluğu">Göz Altı Morluğu</option>--}}
                                {{--<option value="Çarpık Bacak Estetiği">Çarpık Bacak Estetiği</option>--}}
                                {{--<option value="Dudak Büyütme">Dudak Büyütme</option>--}}
                                {{--<option value="Dudak Küçültme">Dudak Küçültme</option>--}}
                                {{--<option value="Kaş Kaldırma">Kaş Kaldırma</option>--}}
                                {{--<option value="Boyun Germe">Boyun Germe</option>--}}
                                {{--<option value="Yağ Transferi">Yağ Transferi</option>--}}
                                {{--<option value="Organik Saç Ekimi">Organik Saç Ekimi</option>--}}
                                {{--<option value="Saç Ekimi">Saç Ekimi</option>--}}
                                {{--<option value="Bio Saç Ekimi">Bio Saç Ekimi</option>--}}
                                {{--<option value="Kaş Ekimi">Kaş Ekimi</option>--}}
                                {{--<option value="Bıyık Ekimi">Bıyık Ekimi</option>--}}
                                {{--<option value="Saç Prpsi">Saç Prpsi</option>--}}
                                {{--<option value="Saç Mezoterapisi">Saç Mezoterapisi</option>--}}
                                {{--<option value="Sakal ve Favori Ekimi">Sakal ve Favori Ekimi</option>--}}
                                {{--<option value="Yara ve Yanık İzi Ekimi">Yara ve Yanık İzi Ekimi</option>--}}
                                {{--<option value="Jinekomasti">Jinekomasti</option>--}}
                                {{--<option value="Vajina Estetiği">Vajina Estetiği</option>--}}
                                {{--<option value="Kol Germe">Kol Germe</option>--}}
                                {{--<option value="Yağ Aldırma">Yağ Aldırma</option>--}}
                                {{--<option value="Ütüleme Epilasyon">Ütüleme Epilasyon</option>--}}
                                {{--<option value="Zayıflama">Zayıflama</option>--}}
                                {{--<option value="Diyet ve Beslenme">Diyet ve Beslenme</option>--}}
                                {{--<option value="Ozon Terapi">Ozon Terapi</option>--}}
                                {{--<option value="Dövme Silme">Dövme Silme</option>--}}
                                {{--<option value="SkinDna Testi">SkinDna Testi</option>--}}
                                {{--<option value="Diğer Uygulamalar">Diğer Uygulamalar</option>--}}
                                {{--</select>--}}
                            </div>
                            <div class="filter form-group">
                                <label for=" channel">Operasyon Zamanı</label>
                                <select name="operation_time" id="operation_time" class="form-control">
                                    <option value="">Tarih Seç</option>
                                    <option value="Hemen">Hemen</option>
                                    <option value="Bu Ay İçinde">Bu ay içinde</option>
                                    <option value="1 Ay Sonra">1 ay sonra</option>
                                    <option value="Gelecek Ay İçinde">Gelecek ay içinde</option>
                                    <option value="2 Ay İçinde">2 ay içinde</option>
                                    <option value="6 Ay İçinde">6 ay içinde</option>
                                    <option value="6 Ay Sonra">6 ay sonra</option>
                                </select>
                            </div>
                            <div class="filter form-group">
                                <label for=" score">Skor</label>
                                <select name="score" id="score" class="form-control">
                                    <option value="">Skor seç</option>
                                    <option value="1.00">1.00</option>
                                    <option value="1.50">1.50</option>
                                    <option value="2.00">2.00</option>
                                    <option value="2.50">2.50</option>
                                    <option value="3.00">3.00</option>
                                    <option value="3.50">3.50</option>
                                    <option value="4.00">4.00</option>
                                    <option value="4.50">4.50</option>
                                    <option value="5.00">5.00</option>
                                    <option value="5.50">5.50</option>
                                    <option value="6.00">6.00</option>
                                    <option value="6.50">6.50</option>
                                    <option value="7.00">7.50</option>
                                    <option value="4.25">4.25</option>
                                    <option value="4.50">4.50</option>
                                    <option value="4.75">4.75</option>
                                    <option value="5.00">5.00</option>
                                </select>
                            </div>


                            {{--<div class="input-group date form-group">--}}
                                {{--<input type="text" class="form-control" name="created_at" id="created_at" placeholder="Tarih"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>--}}
                            {{--</div>--}}

                            <div class="filter input-group form-group" id="datepicker">
                                <input type="text" class="created_at  input-sm form-control" name="created_at" placeholder="yyyy/dd/mm"/>
                                <span class="input-group-addon">Tarih Aralığı</span>
                                <input type="text" class="created_at  input-sm form-control"  placeholder="yyyy/dd/mm"  />
                            </div>

                            {{--<div class="filter form-group">--}}
                                {{--<label for="created_at">Tarih</label>--}}
                                {{--<input type="text" class="form-control"  name="created_at" id="created_at">--}}
                            {{--</div>--}}

                            <button style="margin-top: 20px;"  type="submit" class=" col-xs-6 col-sm-4 col-md-1 col-lg-1 pull-right ft_btn btn btn-primary col-lg-pull-1">Arama</button>

                            <div style="" class="row tile_count">
                                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-user"></i> Toplam Lead</span>
                                    <div class="count total green">0</div>
                                    <span class="total_bottom"><i class="green">0% </i></span>
                                </div>
                                <div style="display: none" class="col-md-2 col-sm-4 col-xs-6 tile_stats_count c_l">
                                    <span class="count_top"><i class="fa fa-list"></i> Listelenen Lead</span>
                                    <div class="count list">0</div>
                                    <span class="list_bottom"><i class="green"><i
                                                    class="fa fa-sort-asc"></i>0% </i></span>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-md-12 col-sm-12 col-xs-12">
            <a href="{{route('leads.create')}}" class="pull-right btn btn-success">Lead Ekle</a>
            <div class="x_panel">
                <div class="x_content">
                    <table id="datatable-responsive"
                           class="table table-striped table-hover table-bordered dt-responsive nowrap" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Skor</th>
                            <th>Ad Soyad</th>
                            <th>E-posta</th>
                            <th>Kampanya</th>
                            <th>Operasyon Zamanı</th>
                            <th>Şehir</th>
                            <th>Ülke</th>
                            <th>Telefon</th>
                            <th>Mesaj</th>
                            <th>Mecra</th>
                            <th>Konu</th>
                            <th>Arandı mı?</th>
                            <th>Görüşme Durumu</th>
                            <th>Randevu Durumu</th>
                            <th>Randevu</th>
                            <th>Satış Durumu</th>
                            <th>Satış Tutarı</th>
                            <th>Satış Tarihi</th>
                            <th>Form Id</th>
                            <th>Tarih</th>
                            <th>id</th>
                            <th>İşlem</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection



@section('scripts')


    <script type="text/javascript">
        $(document).ready(function () {


            var oTable = $('#datatable-responsive').DataTable({

                dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>" +
                "<'row'<'col-xs-12't>>" +
                "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                processing: true,
                serverSide: true,
                responsive: true,
                "pageLength": 50,
                "searching": false,
                "paging": true,


                ajax: {
                    url: '{{ route('datatable/getdata') }}',

                    data: function (d) {
                        d.name_surname = $('input[name=name_surname]').val();
                        d.phone = $('input[name=phone]').val();
                        d.city = $('select[name=city]').val();
                        d.campaign = $('input[name=campaign]').val();
                        d.created_at = $('input[name=created_at]').val();
                        d.operation_time = $('select[name=operation_time]').val();
                        d.score = $('select[name=score]').val();
                    }
                },

                "order": [[19, "desc"]],

                columns: [
                    {
                        data: 'score', name: 'score',

                        render: function (data, type, row) {

                            if (row["score"] == 0.50) {
//                                $("td:first-child:contains('1')").html()
                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star-half-o fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            } else if (row["score"] == 1.00) {
//                                $("td:first-child:contains('1')").html()
                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            } else if (row["score"] == 1.50) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-half-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            }
                            else if (row["score"] == 2.00) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            } else if (row["score"] == 2.50) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-half-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            }

                            else if (row["score"] == 3.00) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            } else if (row["score"] == 3.50) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-half-o "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            } else if (row["score"] == 4.00) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-o "/>';
                            } else if (row["score"] == 4.50) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star-half-o "/>';
                            } else if (row["score"] == 5.00) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 2, '').display;
                                return numberRenderer(data) +
                                    ' <i style="color:#FF9800" class="fa fa-star fa-lg"/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star "/>' +
                                    ' <i style="color:#FF9800" class="fa fa-star"/>';
                            }


                            return data;
                        }

                    },
                    {data: 'name_surname', name: 'name_surname'},
                    {data: 'email', name: 'email'},
                    {data: 'campaign', name: 'campaign'},
                    {data: 'operation_time', name: 'operation_time'},
                    {data: 'city', name: 'city'},
                    {data: 'country', name: 'country'},
                    {data: 'phone', name: 'phone'},
                    {data: 'message', name: 'message'},
                    {data: 'channel', name: 'channel'},
                    {data: 'subject', name: 'subject'},
                    {
                        data: 'is_called', name: 'is_called',
                        render: function (data, type, row) {
                            if (row["is_called"] == 1) {
                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return numberRenderer(data) + ' <i style="color:#4CAF50" class="fa fa-check-square-o fa-2x"/>';
                            } else if (row["is_called"] == 0) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return numberRenderer(data) + ' <i style="color:#F44336" class="fa fa-ban fa-2x"/>';
                            }
                            return data;
                        }
                    },
                    {data: 'is_interviewed', name: 'is_interviewed'},
                    {data: 'appointment_status', name: 'appointment_status'},
                    {
                        data: 'appointment', name: 'appointment',

                        render: function (data, type, row) {
                            if (row["appointment"] == 1) {
                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return numberRenderer(data) + ' <i style="color:#4CAF50" class="fa fa-check-square-o fa-2x"/>';
                            } else if (row["appointment"] == 0) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return numberRenderer(data) + ' <i style="color:#F44336" class="fa fa-ban fa-2x"/>';
                            }
                            return data;
                        }

                    },
                    {
                        data: 'sales_status', name: 'sales_status',
                        render: function (data, type, row) {
                            if (row["sales_status"] == 1) {
                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return numberRenderer(data) + ' <i style="color:#4CAF50" class="fa fa-check-square-o fa-2x"/>';
                            } else if (row["sales_status"] == 0) {

                                var numberRenderer = $.fn.dataTable.render.number(',', '.', 0, '').display;
                                return numberRenderer(data) + ' <i style="color:#F44336" class="fa fa-ban fa-2x"/>';
                            }
                            return data;
                        }
                    },
                    {data: 'sales_amount', name: 'sales_amount'},
                    {data: 'sales_date', name: 'sales_date'},
                    {data: 'form_id', name: 'form_id'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'leads_id', name: 'leads_id'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}


                ]

            });


//            $('select#city').change( function() {
//
//                oTable.fnFilter($(this).val());
//
//            });

//            $("#end").on(function(){
//                $(".count").text($("#total").text());
//
//            });
//


            $('#search-form').on('submit', function (e) {
                oTable.draw();
                e.preventDefault();

            });
        });
    </script>


    <script>
        function confirmDelete(url) {
            var value = confirm("Bu lead silinecek, emin misiniz?");
            if (value == true) {
                location.href = url;
            }
        }


    </script>

    <script>
        $(document).ready(function () {


//            setTimeout(function(){  location.reload(); }, 90000);


            setTimeout(function () {

                var total = $("#total").text();
                var end = $("#end").text();
                $(".total").text(total);
                $(".total_bottom").html('<span class="count_bottom"><i class="green">' + end + '<i class="fa fa-sort-asc"></i></i>Satır arasında</span>');


            }, 1000);


            function updateFullName() {

                var total = $("#total").text();
                var end = $("#end").text();

                if (total == 0 && end == 0) {

                    $(".list").text(0);
                    $(".list_bottom").text(0);

                } else {
                    $(".list").text(total);
                    $(".list_bottom").html('<span class="total_bottom"><i class="green">' + total + '<i class="fa fa-sort-asc"></i></i>leads filtrelendi</span>');

                }

            }


            $(".ft_btn").click("change keyup", function () {

                $(".c_l").show(100);
                setTimeout(function () {
                    updateFullName();
                }, 500);
            });
            updateFullName()
        });

    </script>

    <script type="text/javascript">
        $(function() {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                language: 'tr',
                "format": "DD/MM/YYYY",

                ranges: {
                    'Bugün': [moment(), moment()],
                    'Dün': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Bu Hafta': [moment().subtract(6, 'days'), moment()],
                    'Bu Ay': [moment().subtract(29, 'days'), moment()],
                    'Bu Yıl': [moment().startOf('month'), moment().endOf('month')],
                    'Geçen Yıl': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },

                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Uygula",
                    "cancelLabel": "Vazgeç",
                    "fromLabel": "Dan",
                    "toLabel": "a",
                    "daysOfWeek": [
                        "Pt",
                        "Sl",
                        "Çr",
                        "Pr",
                        "Cm",
                        "Ct",
                        "Pz"
                    ],
                    "monthNames": [
                        "Ocak",
                        "Şubat",
                        "Mart",
                        "Nisan",
                        "Mayıs",
                        "Haziran",
                        "Temmuz",
                        "Ağustos",
                        "Eylül",
                        "Ekim",
                        "Kasım",
                        "Aralık"
                    ],
                    "firstDay": 1
                }
            }, cb);

            cb(start, end);

        });

        $(".created_at").datepicker({
            clearBtn: true,
            language: "tr",
            keyboardNavigation: false,
            forceParse: false,
            todayHighlight: true,
            format: "yyyy-mm-dd",
            todayBtn: "linked",

        });





    </script>

    </script>


    <style>

        .filter input, select {
            border: none !important;
            border-right: none !important;
            border-top: none !important;
            border-left: none !important;
            box-shadow: none !important;
            border-bottom: 1px dashed #bfb8b8 !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            background: none !important;
            color: black !important;
            border-radius: 4px !important;
            border: 1px solid #828282 !important;
        }

        a.paginate_button, a.paginate_active {
            display: inline-block !important;
            margin-left: 2px !important;
            cursor: pointer !important;
        }

        /*div#datatable-responsive_info {*/
        /*color: #ffffff;*/
        /*background-color: rgba(38,185,154,0.88);*/
        /*border-color: rgba(38,185,154,0.88);*/
        /*padding: 14px;*/
        /*font-size: 17px;*/
        /*border-radius: 5px;*/
        /*margin: 10px 0 10px 0;*/
        /*}*/
    </style>


@endsection


