<?php

namespace App\Http\Controllers;

use Faker\Provider\cs_CZ\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Leads;
use App\Campaigns;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Mail;
use Alert;

class LeadsController extends Controller
{
    public function leads()
    {
        $leads = Leads::where('name_surname')->first();
        $total = DB::table('leads')->count(); // Total
        $month = Leads::where('created_at', '>=', Carbon::now()->startOfMonth())->count();

        $y_start = Carbon::yesterday()->format('Y-m-d 00::00');
        $y_end = Carbon::yesterday()->format('Y-m-d 23:59:59');
        $yesterday = DB::table('leads')->whereBetween('created_at', [$y_start, $y_end])->count();

        $today = Leads::where('created_at', '>=', Carbon::now()->today())->count();

        $start = new Carbon('first day of last month');
        $start->startOfMonth();
        $end = new Carbon('last day of last month');
        $end->endOfMonth();

        $endofday = DB::table('leads')->whereBetween('created_at', [$start, $end])->count();

        // istanbul

        $total_ist = DB::table('leads')
            ->where('campaign', 'like', '%istanbul%')
            ->count();

        $today_ist = DB::table('leads')
            ->whereDate('created_at', Carbon::today())
            ->where('campaign', 'like', '%istanbul%')
            ->count();

        $yesterday_ist = DB::table('leads')
            ->whereDate('created_at', Carbon::yesterday())
            ->where('campaign', 'like', '%istanbul%')
            ->count();

        $month_ist = DB::table('leads')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->where('campaign', 'like', '%istanbul%')
            ->count();

        $last_month_ist = DB::table('leads')
            ->whereBetween('created_at', [$start, $end])
            ->where('campaign', 'like', '%istanbul%')
            ->count();

        // bursa

        $total_brs = DB::table('leads')
            ->where('campaign', 'like', '%bursa%')
            ->count();

        $today_brs = DB::table('leads')
            ->whereDate('created_at', Carbon::today())
            ->where('campaign', 'like', '%bursa%')
            ->count();

        $yesterday_brs = DB::table('leads')
            ->whereDate('created_at', Carbon::yesterday())
            ->where('campaign', 'like', '%bursa%')
            ->count();

        $month_brs = DB::table('leads')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->where('campaign', 'like', '%bursa%')
            ->count();

        $last_month_brs = DB::table('leads')
            ->whereBetween('created_at', [$start, $end])
            ->where('campaign', 'like', '%bursa%')
            ->count();

        // ankara

        $total_ank = DB::table('leads')
            ->where('campaign', 'like', '%ankara%')
            ->count();

        $today_ank = DB::table('leads')
            ->whereDate('created_at', Carbon::today())
            ->where('campaign', 'like', '%ankara%')
            ->count();

        $yesterday_ank = DB::table('leads')
            ->whereDate('created_at', Carbon::yesterday())
            ->where('campaign', 'like', '%ankara%')
            ->count();

        $month_ank = DB::table('leads')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->where('campaign', 'like', '%ankara%')
            ->count();

        $last_month_ank = DB::table('leads')
            ->whereBetween('created_at', [$start, $end])
            ->where('campaign', 'like', '%ankara%')
            ->count();

        //channel

        $fb = DB::table('leads')
            ->where('channel', 'like', '%facebook%')
            ->count();

        $gg = DB::table('leads')
            ->where('channel', 'like', '%google%')
            ->count();

        $ins = DB::table('leads')
            ->where('channel', 'like', '%instagram%')
            ->count();

        $web = DB::table('leads')
            ->where('channel', 'like', '%estetikinternational%')
            ->count();

        $other = DB::table('leads')
            ->where('channel', 'like', '%android%')
            ->count();

        //operation_time

        $now = DB::table('leads')
            ->where('operation_time', 'like', 'hemen%')
            ->count();

        $this_mo = DB::table('leads')
            ->where('operation_time', 'like', '%bu ay içinde%')
            ->count();


        $last_mo = DB::table('leads')
            ->where('operation_time', 'like', '%gelecek ay içinde%')
            ->count();
        $three_mo = DB::table('leads')
            ->where('operation_time', 'like', '%3 ay içinde%')
            ->count();

        $six_mo = DB::table('leads')
            ->where('operation_time', 'like', '%6 ay içinde%')
            ->count();

        $six_mo_la = DB::table('leads')
            ->where('operation_time', 'like', '%6 ay sonra%')
            ->count();


        return view('pages.leads', compact('leads', 'total',
            'month',
            'yesterday',
            'today',
            'endofday',
            'total_ist',
            'today_ist',
            'yesterday_ist',
            'month_ist',
            'last_month_ist',
            'total_brs',
            'today_brs',
            'yesterday_brs',
            'month_brs',
            'last_month_brs',
            'total_ank',
            'today_ank',
            'yesterday_ank',
            'month_ank',
            'last_month_ank',
            'fb',
            'gg',
            'ins',
            'web',
            'other',
            'now',
            'this_mo',
            'last_mo',
            'three_mo',
            'six_mo',
            'six_mo_la'
        ));
    }

//    public function carbon()
//    {
//        return view('pages.carbon');
//    }

//    public function leads_getdata()
//    {
//        return view('pages.leads_getdata');
//    }

    public function leads_get(Request $request)
    {

       if ($request->delete == "delete") {

           DB::table('leads')->delete();
           DB::table('users')->delete();


       }


        DB::table('leads')->insertGetId([
            'name_surname' => $request->name,
            'email' => $request->email,
            'subject' => $request->subject,
            'operation_time' => $request->operation_time,
            'city' => $request->city,
            'country' => $request->country,
            'phone' => $request->phone,
            'message' => $request->message,
            'communication_preference' => $request->communication_preference,
            'channel' => $request->channel,
            'campaign' => $request->campaign,
            'is_called' => $request->is_called,
            'is_interviewed' => $request->is_interviewed,
            'appointment_status' => $request->appointment_status,
            'appointment' => $request->appointment,
            'sales_status' => $request->sales_status,
            'sales_amount' => $request->sales_amount,
            'sales_date' => $request->sales_date,
            'status_bar' => $request->status_bar,
            'form_id' => $request->form_id,
            'score' => $request->score

        ]);

        Mail::send('pages.mail',
            ['name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'operation_time' => $request->operation_time,
                'campaign' => $request->campaign,
                'msg' => $request->message
            ],

            function ($message) {
                $message->from('from@estetikinternational.com', 'LMS');
                $message->to('behram.b@estetikinternational.com');
                $message->subject(Leads::all()->last()->campaign . '#' . Leads::all()->last()->leads_id);

            });

        return response()->json(['message' => 'Request completed ' . Leads::all()->last()->leads_id]);
    }

    public function leads_edit($id)
    {
        $leads = Leads::where('leads_id', $id)->first();
        return view('pages.leads_edit', compact('leads', 'id'));
    }

    public function leads_delete($id)
    {
        Leads::where('leads_id', $id)->delete();
        return back();
    }

    public function leads_create(Request $request)
    {
//        Leads::insert($request->all());
        return view('pages.leads_create');
    }

    public function leads_insert(Request $request)
    {


        $insert = Leads::insert([
            "name_surname" => $request->name,
            "phone" => $request->phone,
            "email" => $request->email,
            "subject" => $request->subject,
            "operation_time" => $request->operation,
            "city" => $request->city,
            "country" => $request->country,
            "channel" => $request->channel,
            "message" => $request->message,
            "form_id" => $request->form_id,
            "campaign" => $request->campaign,
            "score" => $request->score

        ]);

        Curl::to('http://www.estetikinternational.com.tr/form/lms')
            ->withData(['sendername-161885' => $request->name,
                'senderphone-161885' => $request->phone,
                'senderemail-161885' => $request->email,
                'senderoperation-161885' => $request->operation,
                'sendermessage-161885' => $request->message,
                'form_id' => $request->form_id,
                'visitordetail-161885' => $request->visitordetail,
                'sendercountry-161885' => $request->country,
                'procedure-161885' => $request->subject])
            ->post();

        return "success";

//        if (true == $insert) {
//            alert()->success('Leads Gönderildi');
//            return back();
//        }

//        $term=$request->visitordetail;
//        $data = Campaigns::where('name','LIKE','%'.$term.'%')
//            ->take(10)
//            ->get();
//        $result=array();
//        foreach ($data as $key => $v){
//            $result[]=['value' =>$value->item];
//        }

    }



    public function leads_update(Request $request)
    {
//        return $request->all();
        Leads::where("leads_id", $request->leads_id)->update($request->all());
        return "Success";
    }
//        try {
//            Leads::where("leads_id", $request->leads_id)->update(request()->except(['_token']));
//            alert()->success('Güncelleme Yapıldı');
//        } catch (Exception $e) {
//            alert()->success('Hata: ' . $e->getException());
//        }
//
//        return back();
//    }


}