<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use Redirect;
use DataTables;
use App\Leads;
use Carbon\Carbon;


class DataTableController extends Controller
{
//    public function datatable()
//    {
//        return view('datatable');
//
//    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function leads_get_filter_data(Request $request)
    {

        $leads_datatable = DB::table('leads')->select(
            ['leads_id',
                'name_surname',
                'email',
                'subject',
                'operation_time',
                'city',
                'country',
                'phone',
                'message',
                'communication_preference',
                'channel',
                'campaign',
                'is_called',
                'is_interviewed',
                'appointment_status',
                'appointment',
                'sales_status',
                'sales_amount',
                'sales_date',
                'status_bar',
                'form_id',
                'score',
                'created_at'

            ]);





        return Datatables::of($leads_datatable)


            ->filter(function ($query) use ($request) {

                if ($request->has('from') || $request->has('to')) {
                    $from = $request->get('from');
                    $to = $request->get('to');
                    $query->whereBetween('created_at', [$from, $to]);

                }

                if ($request->has('name_surname')) {
                    $query->where('name_surname', 'like', "%{$request->get('name_surname')}%");
                }
                if ($request->has('channel')) {
                    $query->where('channel', 'like', "%{$request->get('channel')}%");
                }
                if ($request->has('city')) {
                    $query->where('city', 'like', "%{$request->get('city')}%");
                }

//                if ($request->has('phone')) {
//                    $query->where('phone', 'like', "%{$request->get('phone')}%");
//                }

                if ($request->has('campaign')) {
                    $query->where('campaign', 'like', "%{$request->get('campaign')}%");
                }

                if ($request->has('operation_time')) {
                    $query->where('operation_time', 'like', "%{$request->get('operation_time')}%");
                }

                if ($request->has('score')) {
                    $query->where('score', 'like', "{$request->get('score')}");
                }

                if ($request->has('created_at')) {
                    $query->where('created_at', 'like', "%{$request->get('created_at')}%");
                }

                if ($request->has('communication_preference')) {
                    $query->where('communication_preference', 'like', "%{$request->get('communication_preference')}%");
                }

            })


            ->addColumn('action', function ($edit) {
                return '<a href="' . route('leads.edit', $edit->leads_id) . '" class="btn btn-xs btn-success"><i class="fa fa-pencil fa-fw"></i> </i> Düzenle</a>
                        <a href="javascript:;" onclick="confirmDelete(\'' . route('leads.delete', $edit->leads_id) . '\');" class=" btn btn-xs btn-danger"> <i class="fa fa-trash-o fa-lg"></i> Sil</a></a>
                ';
            })


            ->make(true);


    }

    function campaigns_get_filter_data ()
    {
        $campaigns_datatable = DB::table('campaigns')->select(
            ['campaign_id',
                'name',
                'url',
                'subject',
                'channel',
                'landing_type',
                'contacted_type',
                'device',
                'months',
                'language',
                'country',
                'city',
                'material',
                'model',
                'budget',
                'sales',
                'conversion_rate',
                'count',
                'slug',
                'created_at'

            ]);


        return Datatables::of($campaigns_datatable)

//            ->filter(function ($query) use ($request) {
//
//                if ($request->has('from') || $request->has('to')) {
//                    $from = $request->get('from');
//                    $to = $request->get('to');
//                    $query->whereBetween('created_at', [$from, $to]);
//
//                },
//
//            })

            ->addColumn('action', function ($edit) {
                return '<a href="' . route('campaign.edit', $edit->campaign_id) . '" class="btn btn-xs btn-success"><i class="fa fa-pencil fa-fw"></i> </i> Düzenle</a>
                        <a href="javascript:;" onclick="confirmDelete(\'' . route('campaign.delete', $edit->campaign_id) . '\');" class=" btn btn-xs btn-danger"> <i class="fa fa-trash-o fa-lg"></i> Sil</a></a>
                ';
            })


            ->make();

    }



}

