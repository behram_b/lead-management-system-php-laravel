<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Leads;
use Illuminate\Support\Facades\Mail;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::auth();
Route::get('/', ['uses' => 'HomeController@home']);

 Route::group(['middleware' => ['web', 'auth', 'permission'] ], function () {
        Route::get('dashboard', ['uses' => 'HomeController@dashboard', 'as' => 'home.dashboard']);
        //users
        Route::resource('user', 'UserController');
        Route::get('user/{user}/permissions', ['uses' => 'UserController@permissions', 'as' => 'user.permissions']);
        Route::post('user/{user}/save', ['uses' => 'UserController@save', 'as' => 'user.save']);
        Route::get('user/{user}/activate', ['uses' => 'UserController@activate', 'as' => 'user.activate']);
        Route::get('user/{user}/deactivate', ['uses' => 'UserController@deactivate', 'as' => 'user.deactivate']);
          Route::post('user/ajax_all', ['uses' => 'UserController@ajax_all']);

        //roles
        Route::resource('role', 'RoleController');
        Route::get('role/{role}/permissions', ['uses' => 'RoleController@permissions', 'as' => 'role.permissions']);
        Route::post('role/{role}/save', ['uses' => 'RoleController@save', 'as' => 'role.save']);
        Route::post('role/check', ['uses' => 'RoleController@check']);


 });


        //Leads
        Route::get('leads', 'LeadsController@leads')->middleware('auth'); ;

        // Datatable Display view
        Route::get('datatable', 'DataTableController@datatable')->middleware('auth');

        Route::post('leads_getdata', 'LeadsController@leads_get')->name('leads_getdata');

        Route::get('leads_getdata', function () {
            return redirect('/dashboard');
        })->middleware('auth');



        Route::get('test', function () {
          echo  Leads::all()->last()->leads_id;
        });


        // Get Data
        Route::get('datatable/leads_getdata', 'DataTableController@leads_get_filter_data')->name('datatable/leads_getdata')->middleware('auth');
        //Edit Leads
        Route::get('leads_edit/{leads_id}', ['uses' => 'LeadsController@leads_edit', 'as' => 'leads.edit'])->middleware('auth');
        //create Data
        Route::get('leads_create/', ['uses' => 'LeadsController@leads_create', 'as' => 'leads.create'])->middleware('auth');
        //insert Data
        Route::post('leads_insert/', ['uses' => 'LeadsController@leads_insert', 'as' => 'leads.insert'])->middleware('auth');
        //Update Data
        Route::post('leads_update/', ['uses' => 'LeadsController@leads_update', 'as' => 'leads.update'])->middleware('auth');
        //Delete Data
        Route::get('leads_delete/{leads_id}', ['uses' => 'LeadsController@leads_delete', 'as' => 'leads.delete'])->middleware('auth');


        //  Campaingns
        Route::get('datatable/campaigns_getdata', 'DataTableController@campaigns_get_filter_data')->name('datatable/campaigns_getdata')->middleware('auth');
        Route::get('campaign_create','CampaignController@campaign_create')->middleware('auth');
        Route::post('campaign_insert',['uses' => 'CampaignController@campaign_insert', 'as' => 'campaign.insert'])->middleware('auth');
        Route::get('campaign_edit/{campaign_id}',['uses' => 'CampaignController@campaign_edit', 'as' => 'campaign.edit']);
        Route::post('campaign_update',['uses' => 'CampaignController@campaign_update', 'as' => 'campaign.update']);
        Route::get('campaign_delete/{campaign_id}',['uses' => 'CampaignController@campaign_delete', 'as' => 'campaign.delete']);
        Route::get('campaigns','CampaignController@campaigns')->middleware('auth');
        //AutoComplete
        Route::get('campaig_autocomplate','CampaignController@campaign_autocomplate')->middleware('auth');

        //Update


