<!-- footer content -->
        <footer>
          <div class="pull-right">
            Estetik İnternational Leads Management
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>


        <!-- jQuery -->
        <script src="{{ URL::asset('/backend/vendors/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('/backend/vendors/jquery_ui/jquery-ui.js') }}"></script>

<!-- Bootstrap -->
        <script src="{{ URL::asset('/backend/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <!-- FastClick -->
        <script src="{{ URL::asset('/backend/vendors/fastclick/lib/fastclick.js') }}"></script>
        <!-- NProgress -->
        <script src="{{ URL::asset('/backend/vendors/nprogress/nprogress.js') }}"></script>

        <!-- datatables -->
        <script src="{{ URL::asset('/backend/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ URL::asset('/backend/vendors/datatables.net/js/dataTables.select.min.js') }}"></script>
        <script src="{{ URL::asset('/backend/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ URL::asset('/backend/vendors/bootstrap-datepicker-1.6.4/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ URL::asset('/backend/vendors/bootstrap-datepicker-1.6.4/locales/bootstrap-datepicker.tr.min.js') }}"></script>




{{--<script src="{{ URL::asset('/backend/vendors/bootstrap-progressbar/bootstrap-progressbar.js') }}"></script>--}}

        {{--<script src="{{ URL::asset('/backend/vendors/moment/moment.js') }}"></script>--}}


        {{--<script src="{{ URL::asset('/backend/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>--}}



        <!-- Custom Theme Scripts -->
        <script src="{{ URL::asset('/backend/build/js/custom.min.js ') }}"></script>

{{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.tr.min.js"></script>--}}



<script type="text/javascript">
      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
    </script>


    @yield('scripts')

  </body>
</html>


