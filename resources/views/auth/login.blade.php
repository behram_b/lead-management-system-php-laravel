@extends('frontLayout.app')
@section('title')
Login
@stop
@section('content')
<div class = "container">
  <div class="wrapper">
    @if (Session::has('message'))
     <div class="alert alert-{{(Session::get('status')=='error')?'danger':Session::get('status')}} " alert-dismissable fade in id="sessions-hide">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <strong>{{Session::get('status')}}!</strong> {!! Session::get('message') !!}
      </div>
    @endif 
    {{ Form::open(array('url' => route('login'), 'class' => 'form-horizontal form-signin','files' => true)) }}    
        <h3 class="form-signin-heading">Leads Management System</h3>
        <div class="logo">
            <img src="images/logo.png" class="img-responsive"  alt="logo">
        </div>
        {!! csrf_field() !!}
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            <div class="col-sm-12">
                {!! Form::text('email', null, ['class' => 'form-control','placeholder '=>'E-mail']) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
            <div class="col-sm-12">
                 {!! Form::password('password', ['class' => 'form-control','placeholder '=>'Şifre']) !!}
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
            </div>
        </div>      
       
        <button class="btn btn-lg btn-primary btn-block submit-btn"  name="Submit" value="Login" type="Submit">Giriş yap</button>

        <div class="login-register">
                {{--<a href="{{url('register')}}">Register</a>--}}
                <a href="{{url('password/reset')}}">Şifremi unuttum</a>
                @if ($errors->has('global'))
                <span class="help-block danger">
                    <strong style="color:red" >{{ $errors->first('global') }}</strong>
                </span>
              @endif 
        </div>     
    </form>
    
  </div>
</div>
@endsection

@section('scripts')


@endsection