
@extends('backLayout.app')


@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Leads Gönder <small>Tüm servislere Lead gönderir</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    {{--{{ Form::open(array('url' => route('leads.insert'), 'class' => 'form-horizontal','files' => true)) }}--}}

                    <form class="form" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-push-4 col-lg-4 col-md-6 col-sm-9 col-xs-12">
                                <label class="" for="visitordetail">* Kampanya </label>
                                <input id="visitordetail" class="form-control col-md-8" data-validate-length-range="6"
                                       data-validate-words="2" name="visitordetail" type="text" required>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-lg-4  col-md-6 col-sm-9 col-xs-12">
                                <label for="name">* Ad-Soyad </label>
                                <input id="name" class="form-control col-md-8" data-validate-length-range="6"
                                       data-validate-words="2" name="name" type="text" required>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-lg-4  col-md-6 col-sm-9 col-xs-12">
                                <label for="email">Email </label>
                                <input type="email" id="email" name="email" class="form-control col-md-8">
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-push-4 col-lg-4  col-md-6 col-sm-9 col-xs-12">
                                <label for="phone">* Telefon</label>
                                <input type="number" id="phone" name="phone"  class="form-control col-md-7 col-xs-12" required>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-lg-4  col-md-6 col-sm-9 col-xs-12">
                                <label  for="subject">Konu</label>
                                <select id="subject" name="subject"  class="form-control col-md-7 col-xs-12">
                                    <option value="">...</option>
                                    <option value="Microblading">Microblading</option>
                                    <option value="Örümcek Ağı Estetiği">Örümcek Ağı Estetiği</option>
                                    <option value="Organik Saç Ekimi">Organik Saç Ekimi</option>
                                    <option value="Burun Estetiği">Burun Estetiği</option>
                                    <option value="Kepçe Kulak Estetiği">Kepçe Kulak Estetiği</option>
                                    <option value="Meme Büyütme">Meme Büyütme</option>
                                    <option value="Meme Küçültme">Meme Küçültme</option>
                                    <option value="Meme Dikleştirme">Meme Dikleştirme</option>
                                    <option value="Yüz Germe">Yüz Germe</option>
                                    <option value="Göz Kapağı Estetiği">Göz Kapağı Estetiği</option>
                                    <option value="Göz Altı Morluğu">Göz Altı Morluğu</option>
                                    <option value="Çarpık Bacak Estetiği">Çarpık Bacak Estetiği</option>
                                    <option value="Dudak Büyütme">Dudak Büyütme</option>
                                    <option value="Dudak Küçültme">Dudak Küçültme</option>
                                    <option value="Kaş Kaldırma">Kaş Kaldırma</option>
                                    <option value="Boyun Germe">Boyun Germe</option>
                                    <option value="Yağ Transferi">Yağ Transferi</option>
                                    <option value="Organik Saç Ekimi">Organik Saç Ekimi</option>
                                    <option value="Saç Ekimi">Saç Ekimi</option>
                                    <option value="Bio Saç Ekimi">Bio Saç Ekimi</option>
                                    <option value="Kaş Ekimi">Kaş Ekimi</option>
                                    <option value="Bıyık Ekimi">Bıyık Ekimi</option>
                                    <option value="Saç Prpsi">Saç Prpsi</option>
                                    <option value="Saç Mezoterapisi">Saç Mezoterapisi</option>
                                    <option value="Sakal ve Favori Ekimi">Sakal ve Favori Ekimi</option>
                                    <option value="Yara ve Yanık İzi Ekimi">Yara ve Yanık İzi Ekimi</option>
                                    <option value="Jinekomasti">Jinekomasti</option>
                                    <option value="Vajina Estetiği">Vajina Estetiği</option>
                                    <option value="Kol Germe">Kol Germe</option>
                                    <option value="Yağ Aldırma">Yağ Aldırma</option>
                                    <option value="Ütüleme Epilasyon">Ütüleme Epilasyon</option>
                                    <option value="Zayıflama">Zayıflama</option>
                                    <option value="Diyet ve Beslenme">Diyet ve Beslenme</option>
                                    <option value="Ozon Terapi">Ozon Terapi</option>
                                    <option value="Dövme Silme">Dövme Silme</option>
                                    <option value="SkinDna Testi">SkinDna Testi</option>
                                    <option value="Diğer Uygulamalar">Diğer Uygulamalar</option>
                                </select>
                            </div>
                            </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-lg-4 col-md-6 col-sm-9 col-xs-12">
                                <label  for="website">Şehir</label>
                                <select name="city" id="city" class="form-control col-md-7 col-xs-12">
                                    <option value="">...</option>
                                    <option value="Adana">Adana</option>
                                    <option value="Adıyaman">Adıyaman</option>
                                    <option value="Afyon">Afyon</option>
                                    <option value="Ağrı">Ağrı</option>
                                    <option value="Aksaray">Aksaray</option>
                                    <option value="Amasya">Amasya</option>
                                    <option value="Ankara">Ankara</option>
                                    <option value="Antalya">Antalya</option>
                                    <option value="Ardahan">Ardahan</option>
                                    <option value="Artvin">Artvin</option>
                                    <option value="Aydın">Aydın</option>
                                    <option value="Balıkesir">Balıkesir</option>
                                    <option value="Bartın">Bartın</option>
                                    <option value="Batman">Batman</option>
                                    <option value="Bayburt">Bayburt</option>
                                    <option value="Bilecik">Bilecik</option>
                                    <option value="Bingöl">Bingöl</option>
                                    <option value="Bitlis">Bitlis</option>
                                    <option value="Bolu">Bolu</option>
                                    <option value="Burdur">Burdur</option>
                                    <option value="Bursa">Bursa</option>
                                    <option value="Çanakkale">Çanakkale</option>
                                    <option value="Çankırı">Çankırı</option>
                                    <option value="Çorum">Çorum</option>
                                    <option value="Denizli">Denizli</option>
                                    <option value="Diyarbakır">Diyarbakır</option>
                                    <option value="Düzce">Düzce</option>
                                    <option value="Edirne">Edirne</option>
                                    <option value="Elazığ">Elazığ</option>
                                    <option value="Erzincan">Erzincan</option>
                                    <option value="Erzurum">Erzurum</option>
                                    <option value="Eskişehir">Eskişehir</option>
                                    <option value="Gaziantep">Gaziantep</option>
                                    <option value="Giresun">Giresun</option>
                                    <option value="Gümüşhane">Gümüşhane</option>
                                    <option value="Hakkari">Hakkari</option>
                                    <option value="Hatay">Hatay</option>
                                    <option value="Iğdır">Iğdır</option>
                                    <option value="Isparta">Isparta</option>
                                    <option value="İçel">İçel</option>
                                    <option value="İstanbul">İstanbul</option>
                                    <option value="İzmir">İzmir</option>
                                    <option value="Kahramanmaraş">Kahramanmaraş</option>
                                    <option value="Karabük">Karabük</option>
                                    <option value="Karaman">Karaman</option>
                                    <option value="Kars">Kars</option>
                                    <option value="Kastamonu">Kastamonu</option>
                                    <option value="Kayseri">Kayseri</option>
                                    <option value="Kırıkkale">Kırıkkale</option>
                                    <option value="Kırklareli">Kırklareli</option>
                                    <option value="Kırşehir">Kırşehir</option>
                                    <option value="Kilis">Kilis</option>
                                    <option value="Kilis">Kocaeli</option>
                                    <option value="Konya">Konya</option>
                                    <option value="Kütahya">Kütahya</option>
                                    <option value="Malatya">Malatya</option>
                                    <option value="Manisa">Manisa</option>
                                    <option value="Mardin">Mardin</option>
                                    <option value="Muğla">Muğla</option>
                                    <option value="Muş">Muş</option>
                                    <option value="Nevşehir">Nevşehir</option>
                                    <option value="Niğde">Niğde</option>
                                    <option value="Ordu">Ordu</option>
                                    <option value="Osmaniye">Osmaniye</option>
                                    <option value="Rize">Rize</option>
                                    <option value="Sakarya">Sakarya</option>
                                    <option value="Samsun">Samsun</option>
                                    <option value="Siirt">Siirt</option>
                                    <option value="Sinop">Sinop</option>
                                    <option value="Sivas">Sivas</option>
                                    <option value="Şanlıurfa">Şanlıurfa</option>
                                    <option value="Şırnak">Şırnak</option>
                                    <option value="Tekirdağ">Tekirdağ</option>
                                    <option value="Tokat">Tokat</option>
                                    <option value="Trabzon">Trabzon</option>
                                    <option value="Tunceli">Tunceli</option>
                                    <option value="Uşak">Uşak</option>
                                    <option value="Van">Van</option>
                                    <option value="Yalova">Yalova</option>
                                    <option value="Yozgat">Yozgat</option>
                                    <option value="Zonguldak">Zonguldak</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-lg-4  col-md-6 col-sm-9 col-xs-12">
                                <label for="Ülke">Ülke</label>
                                <input type="text" id="country" name="Country" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-lg-4 col-md-6 col-sm-9 col-xs-12">
                                <label for="operation">* Operasyon Tarihi</label>
                                <select type="text"  id="operation" class="form-control col-md-7 col-xs-12" name="operation" required>
                                    <option value="">---</option>
                                    <option value="Hemen">Hemen</option>
                                    <option value="Bu Ay İçinde">Bu ay içinde</option>
                                    <option value="Gelecek Ay içinde">Gelecek ay içinde</option>
                                    <option value="2 Ay İçinde">2 ay içinde</option>
                                    <option value="6 ay içinde">6 ay içinde</option>
                                    <option value="6 ay sonrası">6 ay sonrası</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-lg-4 col-md-6 col-sm-9 col-xs-12">
                                <label for="password2">Mecra</label>
                                <input id="channel" type="text" name="channel"  data-validate-linked="password" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-lg-4 col-md-6 col-sm-9 col-xs-12">
                                <label for="textarea">Mesaj</label>
                                <textarea id="message" name="message" class="form-control col-md-7 col-xs-12"></textarea>
                            </div>
                        </div>
                        <br>
                    <div class="ln_solid"></div>
                    <input hidden type="text" name="form_id" id="form_id" value="161885">

                        <div class="col-md-push-4 col-lg-4 col-md-6 col-sm-9 col-xs-12">
                            <a href="{{url('dashboard')}}" class="btn btn-primary">Vazgeç</a>
                            <input hidden type="text" id="score" name="score">
                            <input name="leads_id" hidden type="text" value="">
                            <button class="submit col-md-push-4 col-md-2 col-sm-9 col-xs-12 btn btn-success">Oluştur</button>
                        </div>
                    </form>
                    <a style="display: none;" href="/leads_create" class="cb btn btn-success col-md-2 col-sm-4 col-xs-6 col-lg-push-4 col-md-push-4">Yeni lead ekle</a>
                    <a style="display: none;" href="/leads" class="cb btn btn-primary col-md-2 col-sm-4 col-xs-4 col-lg-push-4 col-md-push-4">Tüm leadler</a>
                </div>
            </div>
        </div>
    </div>





    @include('frontLayout.script.sweetalert')
    @include('sweet::alert')

@endsection


@section('scripts')


    <script>
        $( function() {

            $( "#visitordetail" ).autocomplete({
                source: '/campaig_autocomplate'
            });
        } );
    </script>

    <script>


        $("input, textarea, select").click(function() {

            name_ = $("#name").val();
            phone_ = $("#phone").val();
            email_ = $("#email").val();
            message_ = $("#message").val();
            operation_section_ = $("select[name='operation'] option:selected").index();


            score_ = 0;

            function harfSayisi(metin) {
                var alfabe = /^[a-zA-ZÂâÎîİıÇçŞşÜüÖöĞğ]/;
                var harfSayisi = 0;

                for (var i = 0; i < metin.length; i++)
                    if (metin[i].match(alfabe))
                        harfSayisi++;

                return harfSayisi;
            }

            var metin = $('#message').val();
            var top = harfSayisi(metin);




            if (name_ != "")
                score_ = score_ + 0.25;

            if (phone_ != "")
                score_ = score_ + 1.00;

            if (email_ != "")
                score_ = score_ + 0.25;

            if (message_ != "")
                score_ = score_ + 0.5;

            if (top > 80)
                score_ = score_ + 0.5;



            switch (operation_section_) {
                case 1: //Hemen
                    score_ = score_ + 2.50;
                    break;
                case 2: //Bu ay içinde
                    score_ = score_ + 2.50;
                    break;
                case 3: //Gelecek ay içinde
                    score_ = score_ + 2.00;
                    break;
                case 4: //3 ay içinde
                    score_ = score_ + 1.50;
                    break;
                case 5: //6 ay içinde
                    score_ = score_ + 1.00;
                    break;
                case 6: //6 ay sonra
                    score_ = score_ + 0.50;
                    break;
            }
            $("#score").val(score_);


        });


        $('.submit').click(function (e) {
            e.preventDefault();


            $.ajax({
                type: "POST",
                dataType: "text",
                url: '/leads_insert',
                data: {
                    campaign: $('#visitordetail').val(),
                    name:  $('#name').val(),
                    email:  $('#email').val(),
                    phone: $('#phone').val(),
                    subject: $('#subject').val(),
                    operation: $('#operation').val(),
                    city: $('#city').val(),
                    country: $('#country').val(),
                    channel: $('#channel').val(),
                    message: $('#message').val(),
                    form_id: $('#form_id').val(),
                    score: $('#score').val()

                },
                success: function(data) {
                    swal("Lead Gönderildi", "", "success");
                    $(".form").hide();
                    $('.cb').show(300);
                },
                error: function(data) {
                    swal("Hata", "Kampanya oluşturulurken sorun oldu, tekrar deneyiniz", "error");
                }
            });


        });


    </script>

@endsection









<!-- Font Awesome -->





