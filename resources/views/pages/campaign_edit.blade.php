@extends('backLayout.app')


@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Kampanya Güncelle <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">


                    {{--{{ Form::open(array('url' => route('campaign.update'), 'class' => 'form-horizontal','files' => true)) }}--}}
                    <form class="form" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="input-group  col-md-4 col-sm-9 col-xs-12 col-lg-push-4">
                            <span class="input-group-btn">
                             <button type="button" class="btn btn-danger">Kampanya Name:</button>
                            </span>
                                <input id="name" name="name" type="text" value="{{$campaign->name}}" class="form-control" required>
                            </div>
                            <div id="res" class="input-group col-lg-4 col-md-6 col-sm-6 col-xs-6 col-lg-push-4">
                                <textarea style="height: 100px; width: 100%; font-size:12px;" id="slug" name="slug" type="text" class="form-control">{{$campaign->slug}}</textarea>
                                <span class="input-group-btn">
                              <button style="height: 100px;" type="button" class="btn btn-primary"><i class="fa fa-chain-broken" aria-hidden="true"></i> Slug
                            </button>
                            </span>
                            </div>

                            <div class="input-group col-md-push-4 col-md-4 col-sm-9 col-xs-12">
                            <span class="input-group-btn">
                             <button type="button" class="btn btn-primary">URL:</button>
                            </span>
                                <input id="url" name="url" type="url" value="{{$campaign->url}}" class="form-control" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-push-4 col-md-4 col-sm-9 col-xs-12">
                                <label for="">İl</label>
                                <select id="city" name="city"  class="form-control">
                                    <option selected  value="{{$campaign->city}}">{{$campaign->city}}</option>
                                    <option value="Genel">Genel</option>
                                    <option value="Adana">Adana</option>
                                    <option value="Adıyaman">Adıyaman</option>
                                    <option value="Afyon">Afyon</option>
                                    <option value="Ağrı">Ağrı</option>
                                    <option value="Aksaray">Aksaray</option>
                                    <option value="Amasya">Amasya</option>
                                    <option value="Ankara">Ankara</option>
                                    <option value="Antalya">Antalya</option>
                                    <option value="Ardahan">Ardahan</option>
                                    <option value="Artvin">Artvin</option>
                                    <option value="Aydın">Aydın</option>
                                    <option value="Balıkesir">Balıkesir</option>
                                    <option value="Bartın">Bartın</option>
                                    <option value="Batman">Batman</option>
                                    <option value="Bayburt">Bayburt</option>
                                    <option value="Bilecik">Bilecik</option>
                                    <option value="Bingöl">Bingöl</option>
                                    <option value="Bitlis">Bitlis</option>
                                    <option value="Bolu">Bolu</option>
                                    <option value="Burdur">Burdur</option>
                                    <option value="Bursa">Bursa</option>
                                    <option value="Çanakkale">Çanakkale</option>
                                    <option value="Çankırı">Çankırı</option>
                                    <option value="Çorum">Çorum</option>
                                    <option value="Denizli">Denizli</option>
                                    <option value="Diyarbakır">Diyarbakır</option>
                                    <option value="Düzce">Düzce</option>
                                    <option value="Edirne">Edirne</option>
                                    <option value="Elazığ">Elazığ</option>
                                    <option value="Erzincan">Erzincan</option>
                                    <option value="Erzurum">Erzurum</option>
                                    <option value="Eskişehir">Eskişehir</option>
                                    <option value="Gaziantep">Gaziantep</option>
                                    <option value="Giresun">Giresun</option>
                                    <option value="Gümüşhane">Gümüşhane</option>
                                    <option value="Hakkari">Hakkari</option>
                                    <option value="Hatay">Hatay</option>
                                    <option value="Iğdır">Iğdır</option>
                                    <option value="Isparta">Isparta</option>
                                    <option value="İçel">İçel</option>
                                    <option value="İstanbul">İstanbul</option>
                                    <option value="İzmir">İzmir</option>
                                    <option value="Kahramanmaraş">Kahramanmaraş</option>
                                    <option value="Karabük">Karabük</option>
                                    <option value="Karaman">Karaman</option>
                                    <option value="Kars">Kars</option>
                                    <option value="Kastamonu">Kastamonu</option>
                                    <option value="Kayseri">Kayseri</option>
                                    <option value="Kırıkkale">Kırıkkale</option>
                                    <option value="Kırklareli">Kırklareli</option>
                                    <option value="Kırşehir">Kırşehir</option>
                                    <option value="Kilis">Kilis</option>
                                    <option value="Kilis">Kocaeli</option>
                                    <option value="Konya">Konya</option>
                                    <option value="Kütahya">Kütahya</option>
                                    <option value="Malatya">Malatya</option>
                                    <option value="Manisa">Manisa</option>
                                    <option value="Mardin">Mardin</option>
                                    <option value="Muğla">Muğla</option>
                                    <option value="Muş">Muş</option>
                                    <option value="Nevşehir">Nevşehir</option>
                                    <option value="Niğde">Niğde</option>
                                    <option value="Ordu">Ordu</option>
                                    <option value="Osmaniye">Osmaniye</option>
                                    <option value="Rize">Rize</option>
                                    <option value="Sakarya">Sakarya</option>
                                    <option value="Samsun">Samsun</option>
                                    <option value="Siirt">Siirt</option>
                                    <option value="Sinop">Sinop</option>
                                    <option value="Sivas">Sivas</option>
                                    <option value="Şanlıurfa">Şanlıurfa</option>
                                    <option value="Şırnak">Şırnak</option>
                                    <option value="Tekirdağ">Tekirdağ</option>
                                    <option value="Tokat">Tokat</option>
                                    <option value="Trabzon">Trabzon</option>
                                    <option value="Tunceli">Tunceli</option>
                                    <option value="Uşak">Uşak</option>
                                    <option value="Van">Van</option>
                                    <option value="Yalova">Yalova</option>
                                    <option value="Yozgat">Yozgat</option>
                                    <option value="Zonguldak">Zonguldak</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-md-4 col-sm-9 col-xs-12">
                                <label  for="subject">Konu</label>
                                <select id="subject" name="subject" class="form-control">
                                    <option selected value="{{$campaign->subject}}">{{$campaign->subject}}</option>
                                    <option value="Microblading">Microblading</option>
                                    <option value="Örümcek Ağı Estetiği">Örümcek Ağı Estetiği</option>
                                    <option value="Organik Saç Ekimi">Organik Saç Ekimi</option>
                                    <option value="Burun Estetiği">Burun Estetiği</option>
                                    <option value="Kepçe Kulak Estetiği">Kepçe Kulak Estetiği</option>
                                    <option value="Meme Büyütme">Meme Büyütme</option>
                                    <option value="Meme Küçültme">Meme Küçültme</option>
                                    <option value="Meme Dikleştirme">Meme Dikleştirme</option>
                                    <option value="Yüz Germe">Yüz Germe</option>
                                    <option value="Göz Kapağı Estetiği">Göz Kapağı Estetiği</option>
                                    <option value="Göz Altı Morluğu">Göz Altı Morluğu</option>
                                    <option value="Çarpık Bacak Estetiği">Çarpık Bacak Estetiği</option>
                                    <option value="Dudak Büyütme">Dudak Büyütme</option>
                                    <option value="Dudak Küçültme">Dudak Küçültme</option>
                                    <option value="Kaş Kaldırma">Kaş Kaldırma</option>
                                    <option value="Boyun Germe">Boyun Germe</option>
                                    <option value="Yağ Transferi">Yağ Transferi</option>
                                    <option value="Organik Saç Ekimi">Organik Saç Ekimi</option>
                                    <option value="Saç Ekimi">Saç Ekimi</option>
                                    <option value="Bio Saç Ekimi">Bio Saç Ekimi</option>
                                    <option value="Kaş Ekimi">Kaş Ekimi</option>
                                    <option value="Bıyık Ekimi">Bıyık Ekimi</option>
                                    <option value="Saç Prpsi">Saç Prpsi</option>
                                    <option value="Saç Mezoterapisi">Saç Mezoterapisi</option>
                                    <option value="Sakal ve Favori Ekimi">Sakal ve Favori Ekimi</option>
                                    <option value="Yara ve Yanık İzi Ekimi">Yara ve Yanık İzi Ekimi</option>
                                    <option value="Jinekomasti">Jinekomasti</option>
                                    <option value="Vajina Estetiği">Vajina Estetiği</option>
                                    <option value="Kol Germe">Kol Germe</option>
                                    <option value="Yağ Aldırma">Yağ Aldırma</option>
                                    <option value="Ütüleme Epilasyon">Ütüleme Epilasyon</option>
                                    <option value="Zayıflama">Zayıflama</option>
                                    <option value="Diyet ve Beslenme">Diyet ve Beslenme</option>
                                    <option value="Ozon Terapi">Ozon Terapi</option>
                                    <option value="Dövme Silme">Dövme Silme</option>
                                    <option value="SkinDna Testi">SkinDna Testi</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-md-4 col-sm-9 col-xs-12">
                                <label for="">Mecra</label>
                                <select id="channel" name="channel" class="form-control">
                                    <option selected value="{{$campaign->channel}}">{{$campaign->channel}}</option>
                                    <option id="gl_adv" value="Google">Google</option>
                                    <option id="fb_adv" value="Facebook">Facebook</option>
                                    <option id="ins_adv" value="Instagram">Instagram</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div  id="model" class="row">
                            <div class="col-md-push-4 col-md-4 col-sm-9 col-xs-12">
                                <select id="models" type="text" name="models" class="form-control col-md-8">
                                    <option value="{{$campaign->model}}">{{$campaign->model}}</option>
                                    <option value="Google Arama Ağı">Arama Ağı</option>
                                    <option value="Google Görüntülü Reklam Ağı">Görüntülü Reklam Ağı</option>
                                    <option value="Facebook-Trafik">Trafik</option>
                                    <option value="Facebook-Etkileşim">Etkileşim</option>
                                    <option value="Facebook-Dönüşüm">Dönüşüm</option>
                                    <option value="Facebook-Marka Bilinirliği">Marka Bilinirliği</option>
                                    <option value="Facebook-Erişim">Erişim</option>
                                    <option value="Facebook-Video Görüntülemeleri">Video Görüntülemeleri</option>
                                    <option value="Facebook-Potansiyel Müşteri Bulma">Potansiyel Müşteri Bulma</option>
                                    <option value="Instagram-Trafik">Trafik</option>
                                    <option value="Instagram-Etkileşim">Etkileşim</option>
                                    <option value="Instagram-Dönüşüm">Dönüşüm</option>
                                    <option value="Instagram-Marka Bilinirliği">Marka Bilinirliği</option>
                                    <option value="Instagram-Erişim">Erişim</option>
                                    <option value="Instagram-Video Görüntülemeleri">Video Görüntülemeleri</option>
                                    <option value="Instagram-Potansiyel Müşteri Bulma">Potansiyel Müşteri Bulma</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-md-4 col-sm-9 col-xs-12">
                                <label for="contacted_type">İletişim Tipi</label>
                                <select id="contacted_type" type="text" name="contacted_type" class="form-control col-md-8" >
                                    <option value="">{{$campaign->contacted_type}}</option>
                                    <option value="Inbound">Inbound</option>
                                    <option value="Form">Form</option>
                                    <option value="Zopim-LiveChat">Zopim-LiveChat</option>
                                    <option value="Instagram-Potansiyel Müşteri Bulma">Potansiyel Müşteri Bulma</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-md-4 col-sm-9 col-xs-12">
                                <label for="landing_type">Landing</label>
                                <select id="landing_type" name="landing_type"  class="form-control">
                                    <option value="{{$campaign->landing_type}}">{{$campaign->landing_type}}</option>
                                    <option value="LP1">LP1</option>
                                    <option value="LP2">LP2</option>
                                    <option value="LP3">LP3</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-md-4 col-sm-9 col-xs-12">
                                <label for="device">Cihaz</label>
                                <select id="device" name="device"  class="form-control">
                                    <option value="{{$campaign->device}}">{{$campaign->device}}</option>
                                    <option>Mobil</option>
                                    <option>Masaüstü</option>
                                    <option>Mobil+Masaüstü</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-md-4 col-sm-9 col-xs-12">
                                <label for="months">Ay</label>
                                <select id="months" name="months" class="form-control">
                                    <option value="{{$campaign->months}}">{{$campaign->months}}</option>
                                    <option value="Ocak">Ocak</option>
                                    <option value="Şubat">Şubat</option>
                                    <option value="Mart">Mart</option>
                                    <option value="Nisan">Nisan</option>
                                    <option value="Mayıs">Mayıs</option>
                                    <option value="Haziran">Haziran</option>
                                    <option value="Temmuz">Temmuz</option>
                                    <option value="Ağustos">Ağustos</option>
                                    <option value="Eylül">Eylül</option>
                                    <option value="Ekim">Ekim</option>
                                    <option value="Kasım">Kasım</option>
                                    <option value="Aralık">Aralık</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-md-4 col-sm-9 col-xs-12">
                                <label for="language">Dil</label>
                                <select id="language" name="language" class="form-control">
                                    <option value="{{$campaign->language}}">{{$campaign->language}}</option>
                                    <option value="Türkçe">Türkçe</option>
                                    <option value="İngilizce">İngilizce</option>
                                    <option value="Arapça">Arapça</option>
                                    <option value="Fransızca">Fransızca</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-push-4 col-md-4 col-sm-9 col-xs-12">
                                <label for="country">Ülke</label>
                                <select id="country" name="country"  class="form-control">
                                    <option value="{{$campaign->country}}">{{$campaign->country}}</option>
                                    <option value="AF">Afganistan</option>
                                    <option value="DE">Almanya</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AG">Antigua ve Barbuda</option>
                                    <option value="AR">Arjantin</option>
                                    <option value="AL">Arnavutluk</option>
                                    <option value="AW">Aruba</option>
                                    <option value="AU">Avustralya</option>
                                    <option value="AT">Avusturya</option>
                                    <option value="AZ">Azerbaycan</option>
                                    <option value="BS">Bahamalar</option>
                                    <option value="BH">Bahreyn</option>
                                    <option value="BD">Bangladeş</option>
                                    <option value="BB">Barbados</option>
                                    <option value="EH">Batı Sahra (MA)</option>
                                    <option value="BE">Belçika</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BJ">Benin</option>
                                    <option value="BM">Bermuda</option>
                                    <option value="BY">Beyaz Rusya</option>
                                    <option value="BT">Bhutan</option>
                                    <option value="AE">Birleşik Arap Emirlikleri</option>
                                    <option value="US">Birleşik Devletler</option>
                                    <option value="GB">Birleşik Krallık</option>
                                    <option value="BO">Bolivya</option>
                                    <option value="BA">Bosna-Hersek</option>
                                    <option value="BW">Botsvana</option>
                                    <option value="BR">Brezilya</option>
                                    <option value="BN">Bruney</option>
                                    <option value="BG">Bulgaristan</option>
                                    <option value="BF">Burkina Faso</option>
                                    <option value="BI">Burundi</option>
                                    <option value="TD">Çad</option>
                                    <option value="KY">Cayman Adaları</option>
                                    <option value="GI">Cebelitarık (GB)</option>
                                    <option value="CZ">Çek Cumhuriyeti</option>
                                    <option value="DZ">Cezayir</option>
                                    <option value="DJ">Cibuti</option>
                                    <option value="CN">Çin</option>
                                    <option value="DK">Danimarka</option>
                                    <option value="CD">Demokratik Kongo Cumhuriyeti</option>
                                    <option value="TL">Doğu Timor</option>
                                    <option value="DO">Dominik Cumhuriyeti</option>
                                    <option value="DM">Dominika</option>
                                    <option value="EC">Ekvador</option>
                                    <option value="GQ">Ekvator Ginesi</option>
                                    <option value="SV">El Salvador</option>
                                    <option value="ID">Endonezya</option>
                                    <option value="ER">Eritre</option>
                                    <option value="AM">Ermenistan</option>
                                    <option value="MF">Ermiş Martin (FR)</option>
                                    <option value="EE">Estonya</option>
                                    <option value="ET">Etiyopya</option>
                                    <option value="FK">Falkland Adaları</option>
                                    <option value="FO">Faroe Adaları (DK)</option>
                                    <option value="MA">Fas</option>
                                    <option value="FJ">Fiji</option>
                                    <option value="CI">Fildişi Sahili</option>
                                    <option value="PH">Filipinler</option>
                                    <option value="FI">Finlandiya</option>
                                    <option value="FR">Fransa</option>
                                    <option value="GF">Fransız Guyanası (FR)</option>
                                    <option value="PF">Fransız Polinezyası (FR)</option>
                                    <option value="GA">Gabon</option>
                                    <option value="GM">Gambiya</option>
                                    <option value="GH">Gana</option>
                                    <option value="GN">Gine</option>
                                    <option value="GW">Gine Bissau</option>
                                    <option value="GD">Grenada</option>
                                    <option value="GL">Grönland (DK)</option>
                                    <option value="GP">Guadeloupe (FR)</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="GG">Guernsey (GB)</option>
                                    <option value="ZA">Güney Afrika</option>
                                    <option value="KR">Güney Kore</option>
                                    <option value="GE">Gürcistan</option>
                                    <option value="GY">Guyana</option>
                                    <option value="HT">Haiti</option>
                                    <option value="IN">Hindistan</option>
                                    <option value="HR">Hırvatistan</option>
                                    <option value="NL">Hollanda</option>
                                    <option value="HN">Honduras</option>
                                    <option value="HK">Hong Kong (CN)</option>
                                    <option value="VG">İngiliz Virjin Adaları</option>
                                    <option value="IQ">Irak</option>
                                    <option value="IR">İran</option>
                                    <option value="IE">İrlanda</option>
                                    <option value="ES">İspanya</option>
                                    <option value="IL">İsrail</option>
                                    <option value="SE">İsveç</option>
                                    <option value="CH">İsviçre</option>
                                    <option value="IT">İtalya</option>
                                    <option value="IS">İzlanda</option>
                                    <option value="JM">Jamaika</option>
                                    <option value="JP">Japonya</option>
                                    <option value="JE">Jersey (GB)</option>
                                    <option value="KH">Kamboçya</option>
                                    <option value="CM">Kamerun</option>
                                    <option value="CA">Kanada</option>
                                    <option value="ME">Karadağ</option>
                                    <option value="QA">Katar</option>
                                    <option value="KZ">Kazakistan</option>
                                    <option value="KE">Kenya</option>
                                    <option value="CY">Kıbrıs</option>
                                    <option value="KG">Kırgızistan</option>
                                    <option value="KI">Kiribati</option>
                                    <option value="CO">Kolombiya</option>
                                    <option value="KM">Komorlar</option>
                                    <option value="CG">Kongo Cumhuriyeti</option>
                                    <option value="KV">Kosova (RS)</option>
                                    <option value="CR">Kosta Rika</option>
                                    <option value="CU">Küba</option>
                                    <option value="KW">Kuveyt</option>
                                    <option value="KP">Kuzey Kore</option>
                                    <option value="LA">Laos</option>
                                    <option value="LS">Lesoto</option>
                                    <option value="LV">Letonya</option>
                                    <option value="LR">Liberya</option>
                                    <option value="LY">Libya</option>
                                    <option value="LI">Lihtenştayn</option>
                                    <option value="LT">Litvanya</option>
                                    <option value="LB">Lübnan</option>
                                    <option value="LU">Lüksemburg</option>
                                    <option value="HU">Macaristan</option>
                                    <option value="MG">Madagaskar</option>
                                    <option value="MO">Makao (CN)</option>
                                    <option value="MK">Makedonya</option>
                                    <option value="MW">Malavi</option>
                                    <option value="MV">Maldivler</option>
                                    <option value="MY">Malezya</option>
                                    <option value="ML">Mali</option>
                                    <option value="MT">Malta</option>
                                    <option value="IM">Man Adası (GB)</option>
                                    <option value="MH">Marshall Adaları</option>
                                    <option value="MQ">Martinique (FR)</option>
                                    <option value="MU">Mauritius</option>
                                    <option value="YT">Mayotte (FR)</option>
                                    <option value="MX">Meksika</option>
                                    <option value="FM">Mikronezya</option>
                                    <option value="EG">Mısır</option>
                                    <option value="MN">Moğolistan</option>
                                    <option value="MD">Moldova</option>
                                    <option value="MC">Monako</option>
                                    <option value="MR">Moritanya</option>
                                    <option value="MZ">Mozambik</option>
                                    <option value="MM">Myanmar</option>
                                    <option value="NA">Namibya</option>
                                    <option value="NR">Nauru</option>
                                    <option value="NP">Nepal</option>
                                    <option value="NE">Nijer</option>
                                    <option value="NG">Nijerya</option>
                                    <option value="NI">Nikaragua</option>
                                    <option value="NO">Norveç</option>
                                    <option value="CF">Orta Afrika Cumhuriyeti</option>
                                    <option value="UZ">Özbekistan</option>
                                    <option value="PK">Pakistan</option>
                                    <option value="PW">Palau</option>
                                    <option value="PA">Panama</option>
                                    <option value="PG">Papua Yeni Gine</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PE">Peru</option>
                                    <option value="PL">Polonya</option>
                                    <option value="PT">Portekiz</option>
                                    <option value="PR">Porto Riko (US)</option>
                                    <option value="RE">Réunion (FR)</option>
                                    <option value="RO">Romanya</option>
                                    <option value="RW">Ruanda</option>
                                    <option value="RU">Rusya</option>
                                    <option value="BL">Saint Barthélemy (FR)</option>
                                    <option value="KN">Saint Kitts ve Nevis</option>
                                    <option value="LC">Saint Lucia</option>
                                    <option value="PM">Saint Pierre ve Miquelon (FR)</option>
                                    <option value="VC">Saint Vincent ve Grenadinler</option>
                                    <option value="WS">Samoa</option>
                                    <option value="SM">San Marino</option>
                                    <option value="ST">São Tomé ve Príncipe</option>
                                    <option value="SN">Senegal</option>
                                    <option value="SC">Seyşeller</option>
                                    <option value="SL">Sierra Leone</option>
                                    <option value="CL">Şili</option>
                                    <option value="SG">Singapur</option>
                                    <option value="RS">Sırbistan</option>
                                    <option value="SK">Slovakya Cumhuriyeti</option>
                                    <option value="SI">Slovenya</option>
                                    <option value="SB">Solomon Adaları</option>
                                    <option value="SO">Somali</option>
                                    <option value="SS">South Sudan</option>
                                    <option value="SJ">Spitsbergen (NO)</option>
                                    <option value="LK">Sri Lanka</option>
                                    <option value="SD">Sudan</option>
                                    <option value="SR">Surinam</option>
                                    <option value="SY">Suriye</option>
                                    <option value="SA">Suudi Arabistan</option>
                                    <option value="SZ">Svaziland</option>
                                    <option value="TJ">Tacikistan</option>
                                    <option value="TZ">Tanzanya</option>
                                    <option value="TH">Tayland</option>
                                    <option value="TW">Tayvan</option>
                                    <option value="TG">Togo</option>
                                    <option value="TO">Tonga</option>
                                    <option value="TT">Trinidad ve Tobago</option>
                                    <option value="TN">Tunus</option>
                                    <option selected value="TR">Türkiye</option>
                                    <option value="TM">Türkmenistan</option>
                                    <option value="TC">Turks ve Caicos</option>
                                    <option value="TV">Tuvalu</option>
                                    <option value="UG">Uganda</option>
                                    <option value="UA">Ukrayna</option>
                                    <option value="OM">Umman</option>
                                    <option value="JO">Ürdün</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="VU">Vanuatu</option>
                                    <option value="VA">Vatikan</option>
                                    <option value="VE">Venezuela</option>
                                    <option value="VN">Vietnam</option>
                                    <option value="WF">Wallis ve Futuna (FR)</option>
                                    <option value="YE">Yemen</option>
                                    <option value="NC">Yeni Kaledonya (FR)</option>
                                    <option value="NZ">Yeni Zelanda</option>
                                    <option value="CV">Yeşil Burun Adaları</option>
                                    <option value="GR">Yunanistan</option>
                                    <option value="ZM">Zambiya</option>
                                    <option value="ZW">Zimbabve</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div id="material" class="row">
                            <div class="col-md-push-4 col-md-6 col-sm-9 col-xs-12">
                                <label for="material">Material</label><br>
                                <input type="text" name="material" class="col-md-8" placeholder="Değer girilmemiş" value="{{$campaign->material}}">
                            </div>
                        </div>
                        <br>
                        <div id="budget" class="row">
                            <div class="col-md-push-4 col-md-6 col-sm-9 col-xs-12">
                                <label for="budget">Bütçe</label><br>
                                <input  type="text" name="budget" class="col-md-8" placeholder="Değer girilmemiş" value="{{$campaign->budget}}">
                            </div>
                        </div>
                        <br>


                        <input id="campaign_id" type="hidden" value="{{$campaign->campaign_id}}">
                        {{--{!! Form::submit('Oluştur', ['class' => 'submit col-md-push-4 col-md-2 col-sm-9 col-xs-12 btn btn-success']) !!}--}}
                        <button class="submit col-md-push-4 col-md-2 col-sm-9 col-xs-12 btn btn-success">Güncelle</button>
                    </form>
                    <a style="display: none;" href="/campaign_create" class="cb btn btn-success col-md-2 col-sm-4 col-xs-4 col-lg-push-4">Yeni kampanya oluştur</a>
                    <a style="display: none;" href="/campaigns" class="cb btn btn-primary col-md-2 col-sm-4 col-xs-4 col-lg-push-4">Tüm kampanyalar</a>
                </div>
            </div>
        </div>
    </div>


@endsection



@include('frontLayout.script.sweetalert')
@include('sweet::alert')


@section('scripts')
    <script>

        jQuery.fn.en = function(type){
            var tr = new Array("ı","ş","ç","ü","ö","ğ","İ","Ş","Ç","Ü","Ö","Ğ");
            var en = new Array("i","s","c","u","o","g","I","S","C","U","O","G");
            var text;
            $(this).each(function(){
                    $this = $(this);
                    (type=='input') ? text = $this.val() :  text =  $this.text();
                    for (var i=0;i<tr .length;i++)
                        text =  text.replace(new RegExp(tr[i],"g"), en[i]);
                    if (type!='input')
                        $(this).text(text);
                }
            );
            return text;
        };

        $('.submit').click(function (e) {
            e.preventDefault();

            var url = $('#url').en('input').toLowerCase().replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-");
            var subject = $("#subject").en('input').toLowerCase().replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-");
            var channel = $("#channel").en('input').toLowerCase().replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-");
            var l_type = $("#landing_type").en('input').toLowerCase().replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-");
            var device = $("#device").en('input').toLowerCase().replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-");
            var contacted_type = $("#contacted_type").en('input').toLowerCase().replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-");
            var months = $("#months").en('input').toLowerCase().replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-");
            var language = $("#language").en('input').toLowerCase().replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-");
            var country = $("#country").en('input').toLowerCase().replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-");
            var city = $("#city").en('input').toLowerCase().replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-");
            var material = $("#material input").en('input').toLowerCase().replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-");
            var model = $("#models").en('input').toLowerCase().replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-");
            var budget = $("#budget input").en('input').toLowerCase().replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-").replace(" ","-");

            url = url + "?"  + "subject" + "=" + subject +
                "&" + "channel" + "="+ channel +
                "&" + "l_type" + "="+ l_type +
                "&" + "l_device" + "="+ device +
                "&" + "contacted_type" + "="+ contacted_type +
                "&" + "months" + "="+ months +
                "&" + "language" + "="+ language +
                "&" + "country" + "="+ country +
                "&" + "city" + "="+ city +
                "&" + "material" + "="+ material +
                "&" + "model" + "="+ model +
                "&" + "budget" + "="+ budget;


            $.ajax({
                type: "POST",
                dataType: "text",
                url: '/campaign_update',
                data: {
                    campaign_id: $('#campaign_id').val(),
                    name:  $('#name').val(),
                    url: $('#url').val(),
                    subject: $('#subject').val(),
                    channel: $('#channel').val(),
                    landing_type: $('#landing_type').val(),
                    device: $('#device').val(),
                    contacted_type: $('#contacted_type').val(),
                    months: $('#months').val(),
                    language: $('#language').val(),
                    country: $('#country').val(),
                    city: $('#city').val(),
                    material: $('#material').val(),
                    model: model,
                    budget: $('#budget').val(),
                    sales: $('#sales').val(),
                    slug: $('#slug').val()
                },
                success: function(data) {
                    swal("Kampanya Oluşturuldu", "", "success");
                    $(".form").hide()
                    $('#res,.cb').show(300);
                },
                error: function(data) {
                    swal("Hata", "Kampanya oluşturulurken sorun oldu, tekrar deneyiniz", "error");
                }
            });


        });
    </script>
    @endsection