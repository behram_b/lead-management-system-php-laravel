
@extends('backLayout.app')
@section('title')
Login
@stop

@section('style')

@stop

@section('content')
  <div class="row top_tiles">
    <div class="animated flipInY col-lg-2 col-md-2 col-sm-4 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-user"></i></div>
        <div class="count green">{{$total}}</div>
        <h3>Toplam Lead</h3>
      </div>
    </div>
    <div class="animated flipInY col-lg-2 col-md-2 col-sm-4 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-chevron-circle-up"></i></div>
        <div class="count">{{$today}}</div>
        <h3>Bugün</h3>
      </div>
    </div>
    <div class="animated flipInY col-lg-2 col-md-2 col-sm-4 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-chevron-circle-left"></i></div>
        <div class="count">{{$yesterday}}</div>
        <h3>Dün</h3>
      </div>
    </div>
    <div class="animated flipInY col-lg-2 col-md-2 col-sm-4 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-arrow-circle-down"></i></div>
        <div class="count">{{$month}}</div>
        <h3>Bu ay</h3>
      </div>
    </div>
    <div class="animated flipInY col-lg-2 col-md-2 col-sm-4 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-chevron-circle-right"></i></div>
        <div class="count">{{$last_month}}</div>
        <h3>Geçen ay</h3>
      </div>
    </div>
    <div class="animated flipInY col-lg-2 col-md-2 col-sm-4 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-check-square-o"></i></div>
        <div class="count">{{$mont_avg}}</div>
        <h3>Aylık ortalama</h3>
      </div>
    </div>
  </div>


          <!-- /top tiles -->

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                {{--<div class="row x_title">--}}
                  {{--<div class="col-md-6">--}}
                    {{--<h3>Network Activities <small>Graph title sub-title</small></h3>--}}
                  {{--</div>--}}
                  {{--<div class="col-md-6">--}}
                    {{--<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">--}}
                      {{--<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>--}}
                      {{--<span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>--}}
                    {{--</div>--}}
                  {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-md-9 col-sm-9 col-xs-12">--}}
                  {{--<div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>--}}
                  {{--<div style="width: 100%;">--}}
                    {{--<div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height:270px;"></div>--}}
                  {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-sm-3 col-xs-12 bg-white">--}}
                  {{--<div class="x_title">--}}
                    {{--<h2>Top Campaign Performance</h2>--}}
                    {{--<div class="clearfix"></div>--}}
                  {{--</div>--}}

                  {{--<div class="col-md-12 col-sm-12 col-xs-6">--}}
                    {{--<div>--}}
                      {{--<p>Facebook Campaign</p>--}}
                      {{--<div class="">--}}
                        {{--<div class="progress progress_sm" style="width: 76%;">--}}
                          {{--<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="80"></div>--}}
                        {{--</div>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                    {{--<div>--}}
                      {{--<p>Twitter Campaign</p>--}}
                      {{--<div class="">--}}
                        {{--<div class="progress progress_sm" style="width: 76%;">--}}
                          {{--<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="60"></div>--}}
                        {{--</div>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                  {{--</div>--}}
                  {{--<div class="col-md-12 col-sm-12 col-xs-6">--}}
                    {{--<div>--}}
                      {{--<p>Conventional Media</p>--}}
                      {{--<div class="">--}}
                        {{--<div class="progress progress_sm" style="width: 76%;">--}}
                          {{--<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="40"></div>--}}
                        {{--</div>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                    {{--<div>--}}
                      {{--<p>Bill boards</p>--}}
                      {{--<div class="">--}}
                        {{--<div class="progress progress_sm" style="width: 76%;">--}}
                          {{--<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>--}}
                        {{--</div>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                  {{--</div>--}}

                {{--</div>--}}

                {{--<div class="clearfix"></div>--}}
              {{--</div>--}}
            {{--</div>--}}

          {{--</div>--}}
          <br />

          <div class="row">


            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2 class="green">İstanbul</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-12 hidden-small">

                  <table class="countries_list">
                    <tbody>
                    <tr>
                      <td>Bugüne kadar</td>
                      <td class="fs15 fw700 text-right green">{{$total_ist}}</td>
                    </tr>
                    <tr>
                      <td>Bugün</td>
                      <td class="fs15 fw700 text-right green">{{$today_ist}}</td>
                    </tr>
                    <tr>
                      <td>Dün</td>
                      <td class="fs15 fw700 text-right green">{{$yesterday_ist}}</td>
                    </tr>
                    <tr>
                      <td>Bu ay</td>
                      <td class="fs15 fw700 text-right green">{{$month_ist}}</td>
                    </tr>
                    <tr>
                      <td>Geçen ay</td>
                      <td class="fs15 fw700 text-right green">{{$last_month_ist}}</td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320 overflow_hidden">
                <div class="x_title">
                  <h2 class="green">Bursa</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-12 hidden-small">
                  <table class="countries_list">
                    <tbody>
                    <tr>
                      <td>Bugüne kadar</td>
                      <td class="fs15 fw700 text-right green">{{$total_brs}}</td>
                    </tr>
                    <tr>
                      <td>Bugün</td>
                      <td class="fs15 fw700 text-right green">{{$today_brs}}</td>
                    </tr>
                    <tr>
                      <td>Dün</td>
                      <td class="fs15 fw700 text-right green">{{$yesterday_brs}}</td>
                    </tr>
                    <tr>
                      <td>Bu ay</td>
                      <td class="fs15 fw700 text-right green">{{$month_brs}}</td>
                    </tr>
                    <tr>
                      <td>Geçen ay</td>
                      <td class="fs15 fw700 text-right green">{{$last_month_brs}}</td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>


            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320 overflow_hidden">
                <div class="x_title">
                  <h2 class="green">Ankara</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-12 hidden-small">
                  <table class="countries_list">
                    <tbody>
                    <tr>
                      <td>Bugüne kadar</td>
                      <td class="fs15 fw700 text-right green">{{$total_ank}}</td>
                    </tr>
                    <tr>
                      <td>Bugün</td>
                      <td class="fs15 fw700 text-right green">{{$today_ank}}</td>
                    </tr>
                    <tr>
                      <td>Dün</td>
                      <td class="fs15 fw700 text-right green">{{$yesterday_ank}}</td>
                    </tr>
                    <tr>
                      <td>Bu ay</td>
                      <td class="fs15 fw700 text-right green">{{$month_ank}}</td>
                    </tr>
                    <tr>
                      <td>Geçen ay</td>
                      <td class="fs15 fw700 text-right green">{{$last_month_ank}}</td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>

                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="x_panel tile fixed_height_320">
                      <div class="x_title">
                        <h2>Mecra - Bu yıl</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>Facebook</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$fb / 100 }}%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>{{$fb}}</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>

                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>Instagram</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-default" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color: purple; width: {{$ins / 100 }}%;">
                                <span class="sr-only"></span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>{{$ins}}</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>Google</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-red" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$gg / 100 }}%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>{{$gg}}</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>Estetik Int.</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$web / 100 }}%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>{{$web}}</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>Diğer</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color: black;  width: {{$other / 100 }}%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>{{$other}}</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="x_panel tile fixed_height_320 overflow_hidden">
                      <div class="x_title">
                        <h2>Operasyon Tarihi - Bu yıl</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <table class="" style="width:100%">
                          <tbody><tr>
                            <th style="width:37%;">
                              {{--<p>Score   @if ( $score > 400000 and $score < 500000)--}}
                                  {{--<i style="color:#FF9800" class="fa fa-star " aria-hidden="true"></i>--}}
                                  {{--<i style="color:#FF9800" class="fa fa-star" aria-hidden="true"></i>--}}
                                  {{--<i style="color:#FF9800" class="fa fa-star" aria-hidden="true"></i>--}}
                                  {{--<i style="color:#FF9800" class="fa fa-star" aria-hidden="true"></i>--}}
                                  {{--<i style="color:#FF9800" class="fa fa-star-half-o" aria-hidden="true"></i>--}}
                                  {{--<br>--}}
                                  {{--{{$score}}--}}
                                {{--@endif </p>--}}
                              {{--<p>Score ortalama</p>--}}
                              {{--{{$score/100000}}--}}
                            </th>

                            <th>
                              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                <p class="">Operasyon</p>
                              </div>
                              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <p class="">Bu yıl</p>
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <td>
                              <canvas class="canvasDoughnut" height="140" width="20" style="margin: 15px 10px 10px 0"></canvas>
                            </td>
                            <td>
                              <table class="tile_info">
                                <tbody><tr>
                                  <td>
                                    <p><i class="fa fa-square green"></i>Hemen</p>
                                  </td>
                                  <td>{{$now}}</td>
                                </tr>
                                <tr>
                                  <td>
                                    <p><i class="fa fa-square purple"></i>Bu ay içinde </p>
                                  </td>
                                  <td>{{$this_mo}}</td>
                                </tr>
                                <tr>
                                  <td>
                                    <p><i class="fa fa-square"></i>Gelecek ay içinde </p>
                                  </td>
                                  <td>{{$last_mo}}</td>
                                </tr>

                                <tr>
                                  <td>
                                    <p><i class="fa fa-square blue"></i>3 ay içinde </p>
                                  </td>
                                  <td>{{$three_mo}}</td>
                                </tr>
                                <tr>
                                  <td>
                                    <p><i class="fa fa-square red"></i>6 ay içinde </p>
                                  </td>
                                  <td>{{$six_mo}}</td>
                                </tr>
                                <tr>
                                  <td>
                                    <p><i class="fa fa-square red"></i>6 ay sonra </p>
                                  </td>
                                  <td>{{$six_mo_la}}</td>
                                </tr>
                                </tbody></table>
                            </td>
                          </tr>
                          </tbody></table>

                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="x_panel tile fixed_height_320 overflow_hidden">
                      <div class="x_title">
                        <h2>Skor</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <table class="" style="width:100%">
                          <tbody><tr>
                            <th style="width:37%;">
                              <p>Top 5</p>
                            </th>
                            <th>
                              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                <p class="">Yıldız</p>
                              </div>
                              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <p class="">Bu yıl</p>
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <td>
                              <canvas class="canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                            </td>
                            <td>
                              <table class="tile_info">
                                <tbody><tr>
                                  <td>
                                    <p>
                                      <i style="color:#FF9800" class="fa fa-star " aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star" aria-hidden="true"></i>
                                    </p>
                                  </td>
                                  <td>{{$one}}</td>
                                </tr>
                                <tr>
                                  <td>
                                    <p>
                                      <i style="color:#FF9800" class="fa fa-star " aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star-o" aria-hidden="true"></i>
                                    </p>
                                  </td>
                                  <td>{{$two}}</td>
                                </tr>
                                <tr>
                                  <td>
                                    <p>
                                      <i style="color:#FF9800" class="fa fa-star " aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star-o" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star-o" aria-hidden="true"></i>
                                    </p>
                                  </td>
                                  <td>{{$three}}</td>
                                </tr>
                                <tr>
                                  <td>
                                    <p>
                                      <i style="color:#FF9800" class="fa fa-star " aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star-o" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star-o" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star-o" aria-hidden="true"></i>
                                    </p>
                                  </td>
                                  <td>{{$four}}</td>
                                </tr>
                                <tr>
                                  <td>
                                    <p>
                                      <i style="color:#FF9800" class="fa fa-star " aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star-o" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star-o" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star-o" aria-hidden="true"></i>
                                      <i style="color:#FF9800" class="fa fa-star-o" aria-hidden="true"></i>
                                    </p>
                                  </td>
                                  <td>{{$five}}</td>
                                </tr>

                                </tbody></table>
                            </td>
                          </tr>
                          </tbody></table>
                      </div>
                    </div>
                  </div>
                    </div>

              </div>
            </div>

          </div>







              @endsection

@section('scripts')





@endsection